﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintTicket
{
    public class ModulePrint
    {
        ArrayList headerLines = new ArrayList();
        ArrayList subHeaderLines = new ArrayList();
        public ArrayList items = new ArrayList();
        public ArrayList itemsMonedas = new ArrayList();
        public ArrayList itemsManuales = new ArrayList();
        ArrayList totales = new ArrayList();
        ArrayList footerLines = new ArrayList();
        private Image headerImage = null;
        private Image bodyImage = null;
        private Image footerImage = null;
        string mensaje_footer = "";
        private bool Usuer = false;
        private bool Underline = false;

        private bool Manual = false;
        private bool Moneda = false;

        int count = 0;

        int maxChar = 40;
        int maxCharDescription = 9;

        int imageHeight = 0;
        int bodyimageHeight = 0;
        const float Margen = 1;

        float leftMargin = Margen;
        float topMargin = 0;

        string fontName = "Arial Black";
        int fontSize = 8;

        Font printFont = null;
        SolidBrush myBrush = new SolidBrush(Color.Black);

        Graphics gfx = null;

        string line = null;

        public ModulePrint()
        {
            //if ( !Properties.Settings.Default.CAJA_EMPRESARIAL )
            Mensaje_footer = "Estimado cliente para dudas y aclaraciones" + "|" + "Favor de comunicarse CDMX 4209-2492 Resto " + "|" + "del Pais 0155-4209-2492. " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            //else
            //    Mensaje_footer = "Estimado cliente para dudas y aclaraciones" + "|" + "Favor de comunicarse en todo Pais 01-800-734-2623." + "|" +  DateTime.Now.ToShortDateString( ) + " " + DateTime.Now.ToShortTimeString( );
        }

        public Image HeaderImage
        {
            get { return headerImage; }
            set { if (headerImage != value) headerImage = value; }
        }

        public Image FooterImage
        {
            get { return footerImage; }
            set { if (footerImage != value) footerImage = value; }
        }
        public int MaxChar
        {
            get { return maxChar; }
            set { if (value != maxChar) maxChar = value; }
        }

        public int MaxCharDescription
        {
            get { return maxCharDescription; }
            set { if (value != maxCharDescription) maxCharDescription = value; }
        }

        public int FontSize
        {
            get { return fontSize; }
            set { if (value != fontSize) fontSize = value; }
        }

        public string FontName
        {
            get { return fontName; }
            set { if (value != fontName) fontName = value; }
        }

        public bool Underline1
        {
            get
            {
                return Underline;
            }

            set
            {
                Underline = value;
            }
        }

        public Image BodyImage
        {
            get
            {
                return bodyImage;
            }

            set
            {
                bodyImage = value;
            }
        }
        public string Mensaje_footer
        {
            get
            {
                return mensaje_footer;
            }

            set
            {
                mensaje_footer = value;
            }
        }

        public void AddHeaderLine(string line)
        {
            //if (line.Length <= 35)\r\n
            while (line.Contains("\r\n"))
            {
                int l_Inicio = line.IndexOf("\r");
                String l_Imprimir = line.Substring(0, l_Inicio);
                headerLines.Add(l_Imprimir);
                line = line.Substring(l_Inicio + 2);
            }
            if (line != "")
                headerLines.Add(line);
        }

        public void AddSubHeaderLine(string line)
        {
            subHeaderLines.Add(line);
        }

        public void AddItem(string cantidad, string item, string price)
        {
            OrderItem newItem = new OrderItem('?');
            if (item.Length > 18)
                items.Add(newItem.GenerateItem(cantidad, item.Substring(0, 18), price));
            else
                items.Add(newItem.GenerateItem(cantidad, item, price));
        }

        public void AddItemUsuario(string cantidad, string item, string price)
        {
            Usuer = true;
            OrderItem newItem = new OrderItem('?');
            if (item.Length > 18)
                items.Add(newItem.GenerateItem(cantidad, item.Substring(0, 18), price));
            else
                items.Add(newItem.GenerateItem(cantidad, item, price));
        }


        public void AddItemMonedas(string cantidad, string item, string price)
        {
            Moneda = true;
            OrderItem newItem = new OrderItem('?');
            if (item.Length > 18)
                itemsMonedas.Add(newItem.GenerateItem(cantidad, item.Substring(0, 18), price));
            else
                itemsMonedas.Add(newItem.GenerateItem(cantidad, item, price));
        }
        public void AddItemManual(string cantidad, string item, string price)
        {
            Manual = true;
            OrderItem newItem = new OrderItem('?');
            if (item.Length > 18)
                itemsManuales.Add(newItem.GenerateItem(cantidad, item.Substring(0, 18), price));
            else
                itemsManuales.Add(newItem.GenerateItem(cantidad, item, price));
        }

        public void AddTotal(string name, string price)
        {
            OrderTotal newTotal = new OrderTotal('?');
            totales.Add(newTotal.GenerateTotal(name, price));
        }

        public void AddFooterLine(string line)
        {
            footerLines.Add(line);
        }

        public void AddFooterLine2(string line, string line2)
        {
            for (int i = line.Length; i < 5; i++)
                line = " " + line;

            footerLines.Add(line + AlignRightText(line2.Length + line.Length) + line2);
        }

        private string AlignRightText(int lenght)
        {
            string espacios = "";
            int spaces = maxChar - lenght;
            for (int x = 0; x < spaces; x++)
                espacios += " ";
            return espacios;
        }

        private string AllingPrices(int lenght)
        {
            string espacios = "";
            int spaces = maxChar + 6 - lenght;
            for (int x = 0; x < spaces; x++)
                espacios += " ";
            return espacios;
        }

        private string DottedLine()
        {
            string dotted = "";
            for (int x = 0; x < maxChar; x++)
                dotted += "=";
            return dotted;
        }

        public bool PrinterExists(string impresora)
        {
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                if (impresora == strPrinter)
                    return true;
            }
            return false;
        }

        public void PrintTicket(string impresora, string p_folioimpresion)
        {

            try
            {
                if (printFont == null)
                    printFont = new Font(fontName, fontSize, FontStyle.Regular);
                PrintDocument pr = new PrintDocument();

                pr.PrinterSettings.PrinterName = impresora;
                pr.DocumentName = p_folioimpresion;
                pr.PrintPage += new PrintPageEventHandler(pr_PrintPage);
                pr.Print();
                imageHeight = 0;
                maxChar = 40;
                count = 0;
            }
            catch
            {

            }
        }

        private void pr_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            gfx = e.Graphics;


            DrawImage();

            DrawHeader();
            DrawSubHeader();
            DrawImageResume();
            if (items.Count > 0)
            {
                if (Usuer)
                    DrawItemsUsuario();
                else
                {
                    if (Underline)
                        DrawItemsX();
                    else
                        DrawItems();


                }
            }

            if (itemsMonedas.Count > 0)
                DrawItemsMonedas();

            if (itemsManuales.Count > 0)
                DrawItemsManuales();


            DrawTotales();
            DrawFooter();
            DrawFooterDetail();

        }

        private float YPosition()
        {
            return topMargin + (count * printFont.GetHeight(gfx) + imageHeight + bodyimageHeight);
        }

        private void DrawFooterImage()
        {


            if (FooterImage != null)
            {
                try
                {
                    gfx.DrawImage(FooterImage, new Point((int)leftMargin + 5, (int)YPosition()));
                    double height = ((double)FooterImage.Height);
                    imageHeight = (int)Math.Round(height) - 10;
                }
                catch (Exception)
                {
                }
            }

        }

        private void DrawImageResume()
        {
            if (BodyImage != null)
            {
                try
                {
                    gfx.DrawImage(BodyImage, new Point((int)leftMargin, (int)YPosition()));
                    double height = ((double)BodyImage.Height);
                    bodyimageHeight = (int)Math.Round(height / 8);
                }
                catch (Exception)
                {
                }
            }

        }

        private void DrawImage()
        {

            if (headerImage != null)
            {
                try
                {
                    gfx.DrawImage(headerImage, new Point((int)leftMargin, (int)YPosition()));
                    double height = ((double)headerImage.Height);
                    imageHeight = (int)Math.Round(height) - 40;
                }
                catch (Exception)
                {
                }
            }
        }

        private void DrawHeader()
        {
            foreach (string header in headerLines)
            {
                if (header.Length > maxChar)
                {
                    int currentChar = 0;
                    int headerLenght = header.Length;

                    while (headerLenght > maxChar)
                    {
                        line = header.Substring(currentChar, maxChar);
                        gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        headerLenght -= maxChar;
                    }
                    line = header;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = header;
                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawSubHeader()
        {
            foreach (string subHeader in subHeaderLines)
            {
                if (subHeader.Length > maxChar)
                {
                    int currentChar = 0;
                    int subHeaderLenght = subHeader.Length;

                    while (subHeaderLenght > maxChar)
                    {
                        line = subHeader;
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        subHeaderLenght -= maxChar;
                    }
                    line = subHeader;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = subHeader;

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;

                    line = DottedLine();

                    gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            DrawEspacio();
        }

        private void DrawItems()
        {
            OrderItem ordIt = new OrderItem('?');

            //gfx.DrawString("CANT  DESCRIPCION           IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            //gfx.DrawString("C DESCRIPCION       P.U.     IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            gfx.DrawString("Cantidad        Denominación           Subtotal  ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            Font l_printFont = new Font("Courier New", 8, FontStyle.Bold);

            foreach (string item in items)
            {
                int l_Cantidad = int.Parse(ordIt.GetItemCantidad(item).Replace(",", ""));
                Double l_Precio = Double.Parse(ordIt.GetItemPrice(item));
                String l_Total = (l_Precio * l_Cantidad).ToString("$#,###,##0.00");
                line = ordIt.GetItemCantidad(item);
                for (int i = line.Length; i < 5; i++)
                    line = " " + line;


                gfx.DrawString(line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                //line = ordIt.GetItemPrice(item);
                line = l_Total;
                line = AlignRightText(line.Length) + line;

                gfx.DrawString("" + line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);
                while (name.Length < maxCharDescription)
                    name = " " + name;
                string l_PrecioUnitario = ordIt.GetItemPrice(item);
                while (l_PrecioUnitario.Length < 15)
                    l_PrecioUnitario = "  " + l_PrecioUnitario;
                leftMargin = Margen;
                if (name.Length > maxCharDescription)
                    gfx.DrawString("             " + name.Substring(0, maxCharDescription), l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                else
                {
                    while (name.Length < maxCharDescription)
                        name = " " + name;
                    gfx.DrawString("             " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                }
                count++;
            }

            leftMargin = Margen;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            //DrawEspacio();
        }
        private void DrawItemsX()
        {
            OrderItem ordIt = new OrderItem('?');

            //gfx.DrawString("CANT  DESCRIPCION           IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            //gfx.DrawString("C DESCRIPCION       P.U.     IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            gfx.DrawString("Cantidad        Denominación           Subtotal  ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            Font l_printFont = new Font("Courier New", 8, FontStyle.Strikeout);

            foreach (string item in items)
            {
                int l_Cantidad = int.Parse(ordIt.GetItemCantidad(item).Replace(",", ""));
                Double l_Precio = Double.Parse(ordIt.GetItemPrice(item));
                String l_Total = (l_Precio * l_Cantidad).ToString("$#,###,##0.00");
                line = ordIt.GetItemCantidad(item);
                for (int i = line.Length; i < 5; i++)
                    line = " " + line;


                gfx.DrawString(line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                //line = ordIt.GetItemPrice(item);
                line = l_Total;
                line = AlignRightText(line.Length) + line;

                gfx.DrawString("" + line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);
                while (name.Length < maxCharDescription)
                    name = " " + name;
                string l_PrecioUnitario = ordIt.GetItemPrice(item);
                while (l_PrecioUnitario.Length < 15)
                    l_PrecioUnitario = "  " + l_PrecioUnitario;
                leftMargin = Margen;
                if (name.Length > maxCharDescription)
                    gfx.DrawString("             " + name.Substring(0, maxCharDescription), l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                else
                {
                    while (name.Length < maxCharDescription)
                        name = " " + name;
                    gfx.DrawString("             " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                }
                count++;
            }

            leftMargin = Margen;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            //DrawEspacio();
        }

        private void DrawItemsUsuario()
        {
            OrderItem ordIt = new OrderItem('?');

            //gfx.DrawString("CANT  DESCRIPCION           IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            //gfx.DrawString("C DESCRIPCION       P.U.     IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            gfx.DrawString("USUARIO      TOTAL      FECHA  ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            Font l_printFont = new Font("Arial", 8, FontStyle.Regular);

            foreach (string item in items)
            {



                line = ordIt.GetItemCantidad(item);

                gfx.DrawString(line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                line = ordIt.GetItemPrice(item);

                line = AlignRightText(line.Length) + line;

                gfx.DrawString("                                     " + line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);

                string l_PrecioUnitario = ordIt.GetItemPrice(item);
                while (l_PrecioUnitario.Length < 15)
                    l_PrecioUnitario = " " + l_PrecioUnitario;
                leftMargin = Margen;
                if (name.Length > maxCharDescription)
                    gfx.DrawString("                           " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                else
                {
                    gfx.DrawString("                            " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                }
                count++;
            }

            leftMargin = Margen;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            //DrawEspacio();
        }


        private void DrawItemsMonedas()
        {
            OrderItem ordIt = new OrderItem('?');

            //gfx.DrawString("CANT  DESCRIPCION           IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            //gfx.DrawString("C DESCRIPCION       P.U.     IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            gfx.DrawString("         DEPOSITOS DE MONEDAS ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            count++;
            gfx.DrawString("USUARIO      TOTAL      FECHA  ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            Font l_printFont = new Font("Arial", 8, FontStyle.Regular);

            foreach (string item in itemsMonedas)
            {



                line = ordIt.GetItemCantidad(item);

                gfx.DrawString(line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                line = ordIt.GetItemPrice(item);

                line = AlignRightText(line.Length) + line;

                gfx.DrawString("                                     " + line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);

                string l_PrecioUnitario = ordIt.GetItemPrice(item);
                while (l_PrecioUnitario.Length < 15)
                    l_PrecioUnitario = " " + l_PrecioUnitario;
                leftMargin = Margen;
                if (name.Length > maxCharDescription)
                    gfx.DrawString("                           " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                else
                {
                    gfx.DrawString("                            " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                }
                count++;
            }

            leftMargin = Margen;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            //DrawEspacio();
        }

        private void DrawItemsManuales()
        {
            OrderItem ordIt = new OrderItem('?');

            //gfx.DrawString("CANT  DESCRIPCION           IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            //gfx.DrawString("C DESCRIPCION       P.U.     IMP", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            gfx.DrawString("          DEPOSITOS MANUALES ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            count++;
            gfx.DrawString("USUARIO      TOTAL      FECHA  ", printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            DrawEspacio();

            Font l_printFont = new Font("Arial", 8, FontStyle.Regular);

            foreach (string item in itemsManuales)
            {



                line = ordIt.GetItemCantidad(item);

                gfx.DrawString(line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                line = ordIt.GetItemPrice(item);

                line = AlignRightText(line.Length) + line;

                gfx.DrawString("                                     " + line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                string name = ordIt.GetItemName(item);

                string l_PrecioUnitario = ordIt.GetItemPrice(item);
                while (l_PrecioUnitario.Length < 15)
                    l_PrecioUnitario = " " + l_PrecioUnitario;
                leftMargin = Margen;
                if (name.Length > maxCharDescription)
                    gfx.DrawString("                           " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                else
                {
                    gfx.DrawString("                            " + name, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                }
                count++;
            }

            leftMargin = Margen;
            DrawEspacio();
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
            //DrawEspacio();
        }
        private void DrawTotales()
        {
            OrderTotal ordTot = new OrderTotal('?');

            foreach (string total in totales)
            {
                line = ordTot.GetTotalCantidad(total);
                line = AlignRightText(line.Length) + line;

                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                leftMargin = Margen;

                line = "          " + ordTot.GetTotalName(total);
                gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                count++;
            }
            leftMargin = Margen;
            // DrawEspacio();
            DrawEspacio();
        }

        private void DrawFooter()
        {

            Font l_printFont = new Font("Courier New", 8, FontStyle.Bold);

            foreach (string footer in footerLines)
            {
                if (footer.Length - 1 > maxChar)
                {
                    int currentChar = 0;
                    int footerLenght = footer.Length;

                    while (footerLenght > maxChar)
                    {
                        line = footer;
                        gfx.DrawString(line.Substring(currentChar, maxChar), l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        footerLenght -= maxChar;
                    }
                    line = footer;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = footer;
                    gfx.DrawString(line, l_printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                    count++;
                }
            }
            leftMargin = Margen;
            // DrawEspacio();
        }

        private void DrawFooterDetail()
        {




            //System.Configuration.ConfigurationSettings.AppSettings["DetalleCliente"];

            string[] footer = Mensaje_footer.Split('|');

            int FootCount = footer.Length;
            int FootCountTmp = 0;
            line = DottedLine();

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());
            count++;

            DrawFooterImage();
            while (FootCount > FootCountTmp)
            {
                FontSize = 2;
                maxChar = maxChar + 2;
                if (footer[FootCountTmp].Length > maxChar)
                {
                    int currentChar = 0;
                    int footerLenght = footer[FootCountTmp].Length;

                    while (footerLenght > maxChar)
                    {
                        line = footer[FootCountTmp];
                        gfx.DrawString(line.Substring(currentChar, maxChar), printFont, myBrush, leftMargin, YPosition(), new StringFormat());

                        count++;
                        currentChar += maxChar;
                        footerLenght -= maxChar;
                    }
                    line = footer[FootCountTmp];
                    int l_desplazamiento = (maxChar - footerLenght) / 2;
                    gfx.DrawString(line.Substring(currentChar, line.Length - currentChar), printFont, myBrush, leftMargin + l_desplazamiento, YPosition(), new StringFormat());
                    count++;
                }
                else
                {
                    line = footer[FootCountTmp];
                    int l_desplazamiento = (maxChar - line.Length) / 2;
                    gfx.DrawString(line, printFont, myBrush, leftMargin + l_desplazamiento, YPosition(), new StringFormat());

                    count++;
                }

                FootCountTmp++;
            }

            leftMargin = Margen;
            DrawEspacio();
        }

        private void DrawEspacio()
        {
            line = "";

            gfx.DrawString(line, printFont, myBrush, leftMargin, YPosition(), new StringFormat());

            count++;
        }

        public void Dispose()
        {
            if (headerImage != null)
            {
                HeaderImage.Dispose();
                headerImage.Dispose();
            }
        }

    }

    public class OrderItem
    {
        char[] delimitador = new char[] { '?' };

        public OrderItem(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetItemCantidad(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetItemName(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[1];
        }

        public string GetItemPrice(string orderItem)
        {
            string[] delimitado = orderItem.Split(delimitador);
            return delimitado[2];
        }

        public string GenerateItem(string cantidad, string itemName, string price)
        {
            return cantidad + delimitador[0] + itemName + delimitador[0] + price;
        }




    }

    public class OrderTotal
    {
        char[] delimitador = new char[] { '?' };

        public OrderTotal(char delimit)
        {
            delimitador = new char[] { delimit };
        }

        public string GetTotalName(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[0];
        }

        public string GetTotalCantidad(string totalItem)
        {
            string[] delimitado = totalItem.Split(delimitador);
            return delimitado[1];
        }

        public string GenerateTotal(string totalName, string price)
        {
            return totalName + delimitador[0] + price;
        }
    }
}
