﻿using APICI_10;
//using RBG200;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace GSI_API
{
    public class Operation
    {
        private DateTime horaEvento = DateTime.Now;
        public bool ConsultaLineaCaptura(string cuenta,  string referencia,out List<string> infoLinea,bool soloConsulta)
        {
            GSI_WSDL.consultarEstatusLineaCapturaResponse l_sonsultaResponse;
            string respuesta = string.Empty;
            infoLinea = new List<string>();

            try
            {
                PrintLog.iPrintLog("Start_consultarEstatusLineaCaptura");
                PrintLog.iPrintLog(string.Concat("Cajero: ", cuenta, " Linea Captura: ", referencia));
               
                String l_Uri = "http://189.254.76.60:8181/gsiws/ws/cuenta.wsdl";
                AddressHeader l_Head = AddressHeader.CreateAddressHeader("autorizacion", "gsi", Encriptar("hflores" + "|" + "!CE#_[]*!"));
                EndpointAddress l_End = new EndpointAddress(new Uri(l_Uri), l_Head);
                BasicHttpBinding cuentaPort = new BasicHttpBinding();
                cuentaPort.Name = "cuentaPortSoap11";

                GSI_WSDL.cuentaPortClient l_Cliente = new GSI_WSDL.cuentaPortClient(cuentaPort, l_End);
                l_Cliente.Open();
                GSI_WSDL.consultarEstatusLineaCapturaRequest l_Consulta = new GSI_WSDL.consultarEstatusLineaCapturaRequest();
                l_Consulta.clavedispositivo = Encriptar(cuenta);
                l_Consulta.lineacaptura = Encriptar(referencia);
                l_sonsultaResponse = l_Cliente.consultarEstatusLineaCaptura(l_Consulta);

                if(soloConsulta)
                {
                    if(l_sonsultaResponse.estatusLineaCaptura==null)
                    {
                        infoLinea.Add(Operation.Desencriptar(l_sonsultaResponse.tranLineaCaptura));
                        return true;
                    }
                    infoLinea.Add(Operation.Desencriptar(l_sonsultaResponse.estatusLineaCaptura.tranDescripcion));
                    infoLinea.Add(Operation.Desencriptar(l_sonsultaResponse.mensaje));
                    return true;
                }
                else
                {                   
                    if (Operation.Desencriptar(l_sonsultaResponse.estatusLineaCaptura.tranClave).Contains("A"))
                    {
                       return true;
                    }
                    infoLinea.Add(Operation.Desencriptar(l_sonsultaResponse.estatusLineaCaptura.tranDescripcion));
                }

                l_Cliente.Close();
                PrintLog.iPrintLog(string.Concat(new string[] { Operation.Desencriptar(l_sonsultaResponse.estatusLineaCaptura.tranClave), "|", Operation.Desencriptar(l_sonsultaResponse.estatusLineaCaptura.tranDescripcion), "|", Operation.Desencriptar(l_sonsultaResponse.mensaje) }));
                PrintLog.iPrintLog("End_consultarEstatusLineaCaptura");
                return true;
            }
            catch (Exception ex)
            {                
                PrintLog.iPrintLog(ex.Message);
                PrintLog.iPrintLog("End_consultarEstatusLineaCaptura");
                return false;
            }
        }

        public bool PagoLineaCaptura(string cuenta, string referencia, string importe, List<lstDenominaciones> infoDenominaciones, List<lstDenominaciones> infoDenominacionesCambio, string TranOpcion, out string mensaje)
        {
            double cantidad;
            bool flag;
            bool flag1 = false;
            mensaje = string.Empty;
            try
            {
                PrintLog.iPrintLog("Start_pagoLineaCaptura");
                PrintLog.iPrintLog(string.Concat("Cajero: ", cuenta, " Linea Captura: ", referencia));
                string str = "http://189.254.76.60:8181/gsiws/ws/cuenta.wsdl";
                AddressHeader addressHeader = AddressHeader.CreateAddressHeader("autorizacion", "gsi", Operation.Encriptar("hflores|!CE#_[]*!"));
                EndpointAddress endpointAddress = new EndpointAddress(new Uri(str), new AddressHeader[] { addressHeader });
                BasicHttpBinding basicHttpBinding = new BasicHttpBinding()
                {
                    Name = "cuentaPortSoap11"
                };
                GSI_WSDL.cuentaPortClient _cuentaPortClient = new GSI_WSDL.cuentaPortClient(basicHttpBinding, endpointAddress);
                GSI_WSDL.pagarLineaCapturaRequest _pagarLineaCapturaRequest = new GSI_WSDL.pagarLineaCapturaRequest();
                GSI_WSDL.pagarLineaCapturaResponse _pagarLineaCapturaResponse = new GSI_WSDL.pagarLineaCapturaResponse();
                _pagarLineaCapturaRequest.tranBandaMagnetica = Operation.Encriptar("");
                _pagarLineaCapturaRequest.tranCanalOperante = Operation.Encriptar("");
                _pagarLineaCapturaRequest.tranCuentaOrigen = Operation.Encriptar("");
                _pagarLineaCapturaRequest.tranDispositivo = Operation.Encriptar(cuenta);
                _pagarLineaCapturaRequest.tranFecha = Operation.Encriptar(this.horaEvento.ToShortDateString());
                long ticks = this.horaEvento.Ticks;
                _pagarLineaCapturaRequest.tranFolioComprobante = Operation.Encriptar(ticks.ToString());
                _pagarLineaCapturaRequest.tranHora = Operation.Encriptar(this.horaEvento.ToShortTimeString());
                _pagarLineaCapturaRequest.tranImporteOperacion = Operation.Encriptar(importe);
                _pagarLineaCapturaRequest.tranLineaCaptura = Operation.Encriptar(referencia);
                _pagarLineaCapturaRequest.tranOpcion = Operation.Encriptar(TranOpcion);
                _pagarLineaCapturaRequest.tranSecuencia = Operation.Encriptar("1");
                GSI_WSDL.Denominacion[] denominacionArray = new GSI_WSDL.Denominacion[infoDenominaciones.Count<lstDenominaciones>()];
                int num = 0;
                foreach (lstDenominaciones infoDenominacione in infoDenominaciones)
                {
                    GSI_WSDL.Denominacion denominacion = new GSI_WSDL.Denominacion();
                    cantidad = infoDenominacione.Cantidad;
                    denominacion.cantidad = Operation.Encriptar(cantidad.ToString());
                    cantidad = infoDenominacione.Denominacion;
                    denominacion.denominacion = Operation.Encriptar(cantidad.ToString());
                    denominacion.tipo = Operation.Encriptar(infoDenominacione.Item);
                    int num1 = num;
                    num = num1 + 1;
                    denominacionArray[num1] = denominacion;
                }
                _pagarLineaCapturaRequest.listaDenominaciones = denominacionArray;
                GSI_WSDL.Denominacion[] denominacionArray1 = new GSI_WSDL.Denominacion[infoDenominacionesCambio.Count<lstDenominaciones>()];
                num = 0;
                foreach (lstDenominaciones lstDenominacione in infoDenominacionesCambio)
                {
                    GSI_WSDL.Denominacion denominacion1 = new GSI_WSDL.Denominacion();
                    cantidad = lstDenominacione.Cantidad;
                    denominacion1.cantidad = Operation.Encriptar(cantidad.ToString());
                    cantidad = lstDenominacione.Denominacion;
                    denominacion1.denominacion = Operation.Encriptar(cantidad.ToString());
                    denominacion1.tipo = Operation.Encriptar(lstDenominacione.Item);
                    int num2 = num;
                    num = num2 + 1;
                    denominacionArray1[num2] = denominacion1;
                }
                _pagarLineaCapturaRequest.listaDenominacionesCambio = denominacionArray1;
                _pagarLineaCapturaResponse = _cuentaPortClient.pagarLineaCaptura(_pagarLineaCapturaRequest);
                _cuentaPortClient.Close();
                PrintLog.iPrintLog(string.Concat(new string[] { Operation.Desencriptar(_pagarLineaCapturaResponse.estatusLineaCaptura.tranClave), "|", Operation.Desencriptar(_pagarLineaCapturaResponse.estatusLineaCaptura.tranDescripcion), "|", Operation.Desencriptar(_pagarLineaCapturaResponse.tranLineaCaptura) }));
                PrintLog.iPrintLog("End_pagoLineaCaptura");
                flag = flag1;
            }
            catch (Exception exception)
            {
                PrintLog.iPrintLog(exception.Message);
                PrintLog.iPrintLog("End_pagoLineaCaptura");
                flag = flag1;
            }
            return flag;
        }

        //public bool DepositoGsi(List<lstNote> l_detalle, string cuenta, string referencia, out string clave, out string cr, out string letra)
        //{
        //    try
        //    {
        //        //System.Net.ServicePointManager.ServerCertificateValidationCallback =
        //        //((sender, certificate, chain, sslPolicyErrors) => true);

        //        // String l_Uri = "http://189.254.76.60:8443/gsiws/ws/cuenta.wsdl";

        //        String l_Uri = "http://189.254.76.60:8181/gsiws/ws/cuenta.wsdl";


        //        AddressHeader l_Head = AddressHeader.CreateAddressHeader("autorizacion", "gsi", Encriptar("hflores" + "|" + "!CE#_[]*!"));
        //        EndpointAddress l_End = new EndpointAddress(new Uri(l_Uri), l_Head);
        //        BasicHttpBinding cuentaPort = new BasicHttpBinding();
        //        cuentaPort.Name = "cuentaPortSoap11";

        //        GSI_WSDL.cuentaPortClient l_Cliente = new GSI_WSDL.cuentaPortClient(cuentaPort, l_End);
        //        //try
        //        //{
        //        //    l_Cliente.InnerChannel.OperationTimeout = new TimeSpan(0, 1, 0);
        //        //}
        //        //catch (Exception exx)
        //        //{
        //        //   // Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI", "Definiendo tiempo de Envio Deposito: " + exx.Message, 3);
        //        //}



        //        GSI_WSDL.solDepRequest l_solDeposito = new GSI_WSDL.solDepRequest();

        //        l_solDeposito.cuenta = Encriptar(cuenta);
        //        l_solDeposito.idCajero = Encriptar("GSI987");
        //        l_solDeposito.divisa = Encriptar("MXP");
        //        l_solDeposito.fecha = Encriptar(DateTime.Now.ToString("yyyy-MM-dd"));
        //        l_solDeposito.hora = Encriptar(DateTime.Now.ToString("HH:mm:ss"));
        //        l_solDeposito.numeroSecuencia = Encriptar(DateTime.Now.ToString("yyyyMMddHHmmss"));
        //        l_solDeposito.referencia = Encriptar(referencia);
        //        l_solDeposito.folioComprobante = Encriptar("5456");
        //        l_solDeposito.ListaDenominaciones = null;

        //        GSI_WSDL.Denominacion[] lista = new GSI_WSDL.Denominacion[l_detalle.Count];

        //        int i = 0;
        //        foreach (var l_Detalle in l_detalle)
        //        {
        //            GSI_WSDL.Denominacion l_denominacion = new GSI_WSDL.Denominacion();

        //            l_denominacion.cantidad = Encriptar(l_Detalle.Count);
        //            l_denominacion.denominacion = Encriptar(l_Detalle.Note.Replace('B', ' ').Trim());
        //            //if ((int)l_Detalle[0] < 29)
        //            l_denominacion.tipo = Encriptar("B");
        //            //else
        //            //    l_denominacion.tipo = Encriptar("M");

        //            lista[i++] = l_denominacion;
        //        }


        //        l_solDeposito.ListaDenominaciones = lista;

        //        //if (!String.IsNullOrWhiteSpace(p_Usuario))
        //        //l_solDeposito.usuario = Encriptar("01");

        //        l_Cliente.Open();



        //        //Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI", "Enviando Deposito: ", 3);

        //        GSI_WSDL.solDepResponse l_depositoResponse = l_Cliente.solDep(l_solDeposito);




        //        l_Cliente.Close();

        //        // cBitacora.EscribirBitacora("SendGIS", "Clave de Rastreo:", Desencriptar(l_depositoResponse.claveRastreo));

        //        //Globales.EscribirBitacora("GsiPortal", "Envio DepositoGSI", "Deposito Enviado: " + Desencriptar(l_depositoResponse.mensaje), 3);


        //        //Print.printTicket(Desencriptar(l_depositoResponse.claveRastreo), 1);
        //        cr = Desencriptar(l_depositoResponse.cr);
        //        clave = Desencriptar(l_depositoResponse.claveRastreo);
        //        letra = Desencriptar(l_depositoResponse.importeLetra);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        letra = string.Empty;
        //        cr = string.Empty;
        //        clave = string.Empty;
        //        return false;
        //    }
        //}

        public static String Encriptar(String l_data)
        {
            string cipherdata = l_data;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(cipherdata);

            string key = "HG58YZ3CR9";

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.CBC;
            tdes.Padding = PaddingMode.PKCS7;
            tdes.IV = new byte[tdes.BlockSize / 8];



            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            string Resutl = Convert.ToBase64String(resultArray, 0, resultArray.Length);

            return Resutl;
        }

        public static String Desencriptar(string p_mensaje)
        {
            try
            {
                string cipherString = p_mensaje;

                byte[] keyArray;
                byte[] toEncryptArray = Convert.FromBase64String(cipherString);

                string key = "HG58YZ3CR9";

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.CBC;
                tdes.Padding = PaddingMode.PKCS7;
                tdes.IV = new byte[tdes.BlockSize / 8];

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                tdes.Clear();
                string Resutl = UTF8Encoding.UTF8.GetString(resultArray);

                return Resutl;
            }
            catch (Exception)
            {

                return "";

            }
        }

        public static bool LoginGsi(string p_cuenta, out string p_mensaje, out string p_nombre, out string p_monto)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
    ((sender, certificate, chain, sslPolicyErrors) => true);

            try
            {

                String l_Uri = "http://189.254.76.60:8181/gsiws/ws/cuenta.wsdl";

                BasicHttpBinding cuentaPort = new BasicHttpBinding();
                cuentaPort.Name = "cuentaPortSoap11";

                AddressHeader l_Head = AddressHeader.CreateAddressHeader("autorizacion", "gsi", Encriptar("hflores" + "|" + "!CE#_[]*!"));
                EndpointAddress l_End = new EndpointAddress(new Uri(l_Uri), l_Head);

                GSI_WSDL.cuentaPortClient l_Cliente = new GSI_WSDL.cuentaPortClient(cuentaPort, l_End);




                GSI_WSDL.cuentaRequest l_Req = new GSI_WSDL.cuentaRequest();

                l_Req.cuenta = Encriptar(p_cuenta);
                l_Req.idCajero = Encriptar("GSI321");



                l_Cliente.Open();


                GSI_WSDL.cuentaResponse l_R = l_Cliente.cuenta(l_Req);


                l_Cliente.Close();

                p_mensaje = Desencriptar(l_R.mensaje);

                //Globales.GSI_Bitacora("GsiPortal", "Login Cuenta Bancaria: " + p_cuenta, " Resultado: " + p_mensaje, 3);

                if (String.Equals("ACTIVA", Desencriptar(l_R.codigo)))
                {
                    p_nombre = Desencriptar(l_R.titular);
                    p_monto = Desencriptar(l_R.limiteDeposito);
                    return true;
                }

                else
                {
                    p_monto = "99999999";
                    p_nombre = "Luis Angel Juarez Mtz.";
                    return false;
                }




            }
            catch (Exception exx)
            {
                p_mensaje = "Exepcion Encontrada:  " + exx.Message;
                p_monto = "";
                p_nombre = "";
                return false;
            }

        }
    }
}
