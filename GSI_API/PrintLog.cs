﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSI_API
{
    public class PrintLog
    {
        public static void iPrintLog(string data)
        {
            string pathDir = @"c:\LOG_CI-10";
            string pathFile = @"c:\LOG_CI-10\MyLog.txt";
            if (!File.Exists(pathFile))
            {
                DirectoryInfo di = Directory.CreateDirectory(pathDir);
                FileStream sw = File.Create(pathFile);
                sw.Close();
                StreamWriter WriteReportFile = File.AppendText(pathFile);
                WriteReportFile.WriteLine(DateTime.Now + " : " + data);
                WriteReportFile.Close();
            }
            else
            {
                StreamWriter WriteReportFile = File.AppendText(pathFile);
                WriteReportFile.WriteLine(DateTime.Now + " : " + data);
                WriteReportFile.Close();
            }
        }
    }
}
