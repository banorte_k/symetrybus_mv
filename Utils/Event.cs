﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utils
{
    public enum Events
    {
        Success,
        Error,
        Info
    }

    public class Utils
    {
        public delegate void MessageBalloon(string result);
        public static bool notify = true;

        public static string COMMAND_START = "START";
        public static string COMMAND_EXIT = "EXIT";

        public enum Devices
        {
            SCANNER_DESKO,
            FINGERPRINT_READER_UAU
        }

        public static class NotifyEvent
        {
            public static MessageBalloon MessageEvent;
        }
    }

    static class Path
    {

        static public String buildImageFile(String path, String ext)
        {
            String resultPath = "";
            String directoryName;
            String fileName;
            String extName;

            //path = "c:\\x1\\";
            if (path.Length == 0)
            {
                resultPath = Directory.GetCurrentDirectory() + "\\imgCheques\\" + getDefaultFileName(ext);
            }
            else
            {
                directoryName = System.IO.Path.GetDirectoryName(path);
                if (directoryName == null)
                {
                    resultPath = path + getDefaultFileName(ext);
                }
                else
                {
                    if (directoryName.Length == 0)
                    {
                        resultPath = Directory.GetCurrentDirectory() + "\\imgCheques\\" + path;
                    }
                    else
                    {
                        fileName = System.IO.Path.GetFileName(path);
                        resultPath = path;
                        if ((fileName == null) || (System.IO.Path.GetFileName(path).Length == 0))
                        {
                            resultPath = resultPath + getDefaultFileName(ext);
                        }
                        else
                        {
                            extName = System.IO.Path.GetExtension(resultPath);
                            if ((extName == null) || (System.IO.Path.GetExtension(resultPath).Length == 0))
                            {
                                resultPath = resultPath + "\\" + getDefaultFileName(ext);
                            }
                        }
                    }
                }

            }
            resultPath = validatePath(resultPath, System.IO.Path.GetExtension(resultPath).Length == 0);
            return resultPath;
        }

        static private String validatePath(string pathParam, bool isDirectory)
        {

            isDirectory = false;
            String path = pathParam;
            String directoryName;
            Regex driveCheck = new Regex(@"^[a-zA-Z]:\\$");

            if (!driveCheck.IsMatch(path.Substring(0, 3)))
                path = Directory.GetCurrentDirectory();

            string strTheseAreInvalidFileNameChars = new string(System.IO.Path.GetInvalidPathChars());
            strTheseAreInvalidFileNameChars += @":/?*" + "\"";
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

            directoryName = System.IO.Path.GetDirectoryName(path);
            if (directoryName != null)
            {
                DirectoryInfo dir = new DirectoryInfo(System.IO.Path.GetDirectoryName(path));
                if (!dir.Exists)
                    dir.Create();

                if (isDirectory)
                {
                    if (!path.EndsWith("\\"))
                    {
                        path += "\\";
                    }
                }
            }
            return path;
        }

        static private String getDefaultFileName(String ext)
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            System.Threading.Thread.Sleep(1);
            return timeSpan.TotalSeconds.ToString().Replace(".", "") + "." + ext;
        }
    }

    public class Log
    {
        public static TextBox text;
        public static string message = "";

        public static void Output(string msg)
        {
            try
            {
                message = "[" + UnixTimeNow().ToString() + "] " + msg + "\r\n";

                var demoThread =
                    new Thread(new ThreadStart(SetText));
                demoThread.Start();
            }
            catch
            {

            }
        }

        private static void SetText()
        {
            try
            {
                if (text.InvokeRequired)
                {
                    text.Invoke(new MethodInvoker(delegate
                    {
                        text.AppendText(message);
                        text.Refresh();
                    }));
                }
            }
            catch
            {

            }
        }

        public static long UnixTimeNow()
        {
            var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalMilliseconds;
        }
        public static void Write(String p_Modulo, String p_Funcion, string p_Mensaje)
        {
            string l_log = "c:";
            string sArchivo = "SymBusBitacora.txt";
            string sPatch = "";

            l_log = Properties.Settings.Default.path_log;

            try
            {
                Directory.CreateDirectory(l_log);
                //DCS 28Mar2017 Evalua si existe archivo txt y crea si no existe.
                sPatch = l_log + "\\" + sArchivo;
                if (!File.Exists(sPatch))
                {
                    File.Create(sPatch);
                }
                sPatch = l_log + "\\" + sArchivo;
                using (StreamWriter l_Archivo = File.AppendText(sPatch))  //l_log + "\\SymBusBitacora.txt"
                {
                    l_Archivo.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss:fff") + "|" + p_Modulo + "|" + p_Funcion + "|" + p_Mensaje);
                }
            }
            catch (Exception reg)
            {
                l_log = "c:";
                p_Mensaje += " Error en PATH externo: " + reg.Message;
                Log.Output("Error en registro de log: " + reg.Message);
            }
            finally
            {

            }
        }


    }
}
