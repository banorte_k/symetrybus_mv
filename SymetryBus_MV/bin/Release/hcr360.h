#ifndef __HCR360_H__
#define __HCR360_H__
#include "wsc.h"

#ifndef USHORT
#define USHORT unsigned short
#define ULONG  unsigned long
#define WORD   unsigned int
#endif

#ifndef BYTE
#define BYTE  unsigned char
#endif


#define TLP_ACK	0x60
#define TLP_NACK	0xe0
#define TLP_OK		0      //for communication return code

 //Define IC Card VCC
#define V18_EMV	0x11
#define V3_EMV		0x13
#define V5_EMV		0x15

#define V18_ISO	0x01
#define V3_ISO		0x03
#define V5_ISO		0x05


 //Define IC Card VCC
#define V1_8		1
#define V3			3
#define V5			5

#define LED_OFF		0
#define LED_ON		1
#define LED_FLASH	2

#ifdef WIN32
  #ifdef DLL_SOURCE_CODE
     #define DLL_IMPORT_EXPORT __declspec(dllexport) __stdcall
  #else
     #define DLL_IMPORT_EXPORT __declspec(dllimport) __stdcall
  #endif
#else
  #define DLL_IMPORT_EXPORT FAR PASCAL
#endif

#ifdef __cplusplus
  #define NoMangle extern "C"
#else
  #define NoMangle
#endif

//Generic functions for internal used
void SetWaitTime(int Value);
void DefaultWaitTime(void);

NoMangle int DLL_IMPORT_EXPORT OpenCom(int Port, int Baudrate, int Databits);
//NoMangle int  DLL_IMPORT_EXPORT usiOpenReader(int Port);
NoMangle void DLL_IMPORT_EXPORT CloseCom(int Port);
NoMangle void DLL_IMPORT_EXPORT usi2SendData(int Port, BYTE Cmd, USHORT DataLen, BYTE DataBuf[]);
NoMangle int DLL_IMPORT_EXPORT usi2ReceiveData(int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2Exchange(int Port , BYTE Cmd, USHORT SndDataLen, 
										   BYTE SndData[], BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2GetReaderVersion(int Port , BYTE MaskCode[]);
NoMangle int DLL_IMPORT_EXPORT usi2CheckCardPresence(int Port);
NoMangle int DLL_IMPORT_EXPORT usi2CardMoveNotify(int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2CardPowerUp(int Port, BYTE Vcc, BYTE EmvComply, BYTE Atr[]);
NoMangle int DLL_IMPORT_EXPORT usi2CardPowerOff(int Port);
NoMangle int DLL_IMPORT_EXPORT usi2SendApdu(int Port , USHORT ApduLen, BYTE ApduCmd[],
										   BYTE OutData[], BYTE SW[]);
//NoMangle int DLL_IMPORT_EXPORT usiSendMemCardApdu(int Port , USHORT ApduLen, BYTE ApduCmd[],
//												  BYTE OutData[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2Retransmit (int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2ReaderVerReport (int Port, BYTE SubCmd, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2SwitchStatus (int Port);
NoMangle int DLL_IMPORT_EXPORT usi2SetVerboseMode (int Port);
NoMangle int DLL_IMPORT_EXPORT usi2ReaderStatus (int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2SetGreenLed (int Port, BYTE LedStatus);
NoMangle int DLL_IMPORT_EXPORT usi2SetRedLed (int Port, BYTE LedStatus);
NoMangle int DLL_IMPORT_EXPORT usi2Latch (int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2Unlatch (int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2ConfigReport (int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2WarmReset (int Port);

//msg command
NoMangle int DLL_IMPORT_EXPORT usi2ArmtoRead(int Port);
NoMangle int DLL_IMPORT_EXPORT usi2Abort(int Port);
NoMangle int DLL_IMPORT_EXPORT usi2ReadMagData(int Port, BYTE selectTK, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2TK1Data(int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2TK2Data(int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2TK3Data(int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2ISOData(int Port, BYTE Track, BYTE viaISO, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2TransmitErrData(int Port, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2ForwardCusData(int Port, BYTE Track, BYTE viaNull,
												 BYTE chrNums, BYTE RcvData[]);
NoMangle int DLL_IMPORT_EXPORT usi2ReverseCusData(int Port, BYTE Track, BYTE viaNull,
												 BYTE chrNums, BYTE RcvData[]);

 //memory card command
NoMangle int DLL_IMPORT_EXPORT usi2SelectMemCardType (int Port, BYTE MemCardType, BYTE PageSize);
NoMangle int DLL_IMPORT_EXPORT usi2SendMemCardApdu (int Port, USHORT ApduLen, BYTE ApduCmd[], 
												   BYTE RcvData[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2ReportMemCardType (int Port);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryVerify (int Port, USHORT Nums,BYTE PassWord[],BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryAuthenticate (int Port, USHORT Nums, BYTE DataBuf[],
													  BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryExtAuthenticate (int Port, USHORT Nums,
														 BYTE DataBuf[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryReadData (int Port, USHORT Addr, USHORT Nums, 
												  BYTE RcvData[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryWriteData (int Port, USHORT Addr, USHORT Nums,
												   BYTE DataBuf[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryEraseData (int Port, USHORT Addr, BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryRestoreData (int Port, USHORT Addr, BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryWriteProtect (int Port, USHORT Addr, USHORT Nums, 
													  BYTE DataBuf[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryReadProtect (int Port, USHORT Addr, USHORT Nums, 
													 BYTE RcvData[], BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryEraseUserArea (int Port, USHORT Nums, BYTE ErsPSC[]
													   , BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryChangePSC (int Port, USHORT Nums, BYTE NewPSC[]
												   , BYTE SW[]);
NoMangle int DLL_IMPORT_EXPORT usi2MemoryReadErrCount (int Port, BYTE RcvData[], BYTE SW[]);

NoMangle void DLL_IMPORT_EXPORT TLP224Turbo_SendData(int Port, BYTE Cmd,
																	BYTE DataLen, BYTE DataBuf[]);
NoMangle void DLL_IMPORT_EXPORT TLP224_SendData(int Port, BYTE Cmd,
																	BYTE DataLen, BYTE DataBuf[]);
void HelfData (int Port,BYTE Data);

NoMangle int DLL_IMPORT_EXPORT TLP224Turbo_Exchange(int Port , BYTE Cmd, BYTE SndDataLen, BYTE SndData[],
									         BYTE RcvData[],int delayTime);
NoMangle int DLL_IMPORT_EXPORT TLP224_Exchange(int Port , BYTE Cmd, BYTE SndDataLen, BYTE SndData[],
									         BYTE RcvData[],int delayTime);
NoMangle int DLL_IMPORT_EXPORT TLP224_ReceiveData(int Port, BYTE RcvData[],int delayTime);
NoMangle int DLL_IMPORT_EXPORT TLP224Turbo_ReceiveData(int Port, BYTE RcvData[],int delayTime);
NoMangle int DLL_IMPORT_EXPORT AutoReceiveData(int Port, BYTE RcvData[]);
NoMangle void DLL_IMPORT_EXPORT SendData(int Port, USHORT DataLen, BYTE DataBuf[]);
NoMangle void DLL_IMPORT_EXPORT SendRawData(int Port, USHORT DataLen, BYTE DataBuf[]);
NoMangle int DLL_IMPORT_EXPORT UIS1_Exchange(int Port , USHORT DataLen,
											BYTE SndData[],BYTE RcvData[],int delayTime);
NoMangle int DLL_IMPORT_EXPORT UIS1_ReceiveData(int Port, BYTE RcvData[], int delayTime);
NoMangle void DLL_IMPORT_EXPORT UIS1_SendData(int Port, USHORT DataLen, BYTE DataBuf[]);


#endif