﻿using Newtonsoft.Json.Linq;
using RBG200;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace SymetryBus_MV.Models
{
    class Message
    {
        public enum TypeMessage
        {
            Error,
            Success,
            Info
        }

        public Events type;
        public string command;
        public string message;
        public Int32 total;
        public List<lstNote> messageArray;
        public GLYRBG200UNITSTATUS statusArray;
        public Task<JObject> setupArray;
        public List<string> datosArray;


        public Message(Events type,
           string command,
           List<string> datosArray)
        {
            this.type = type;
            this.command = command;
            this.datosArray = datosArray;
        }

        public Message(Events type,
            string command,
            Task<JObject> message)
        {
            this.type = type;
            this.command = command;
            this.setupArray = message;
        }

        public Message(Events type,
            string command,
            string message,
            Int32 total)
        {
            this.type = type;
            this.command = command;
            this.message = message;
            this.total = total;
        }

        public Message(Events type,
            string command,
            List<lstNote> message)
        {
            this.type = type;
            this.command = command;
            this.messageArray = message;
        }

        public Message(Events type,
            string command,
            GLYRBG200UNITSTATUS message)
        {
            this.type = type;
            this.command = command;
            this.statusArray = message;
        }
    }
}
