﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SymetryBus_MV.Models
{
    class Threading
    {
        private static Timer SendWithDraw;

        public static Thread hiloSend;

        public static ThreadStart delegadoSend;

        static Threading()
        {
            Threading.delegadoSend = new ThreadStart(Threading.CorrerProcesoSend);
        }

        public Threading()
        {
            Threading.hiloSend = new Thread(Threading.delegadoSend);
            Threading.hiloSend.Start();
        }

        private static void CorrerProcesoSend()
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                UseShellExecute = true,
                Verb = "runas",
                WorkingDirectory = Environment.CurrentDirectory,
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = string.Concat(Environment.ExpandEnvironmentVariables("%SystemRoot%"), "\\System32\\cmd.exe"),
                Arguments = string.Concat("/ user:Administrator \"cmd /K ConsoleRGB200.exe ", WebSocketDispatch.totalRetiro)
            };
            Console.WriteLine(startInfo.Arguments.ToString());
            process.StartInfo = startInfo;
            process.Start();
        }

        private void send(object state)
        {
            Threading.hiloSend = new Thread(Threading.delegadoSend);
            Threading.hiloSend.Start();
        }
    }
}
