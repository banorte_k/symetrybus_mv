﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RBG200;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace SymetryBus_MV.Models
{
    public class MethodPublic
    {
        private readonly HttpClient client = new HttpClient();

        public async Task<JObject> GetSetup(string ns)
        {
            //var response = await client.GetAsync("http://showroom.centralus.cloudapp.azure.com/sam/ws/setup?serial=" + ns);
            var response = await client.GetAsync("http://symetry.cloud/sam/ws/setup?serial=" + ns);


            var responseString = await response.Content.ReadAsStringAsync();

            // JObject.Parse(responseString);
            return JObject.Parse(responseString);
        }

        public async Task<bool> sendTrans(Events type, String command, List<lstNote> data)
        {
            String message = JsonConvert.SerializeObject(new Models.Message(type, command, data), Formatting.Indented);

            var values = new Dictionary<string, string>
            {
               { "deposit", message }
            };

            var content = new FormUrlEncodedContent(values);

            // var response = await client.PostAsync("http://showroom.centralus.cloudapp.azure.com/sam/ws/deposit", content);
            var response = await client.PostAsync("http://symetry.cloud/sam/ws/deposit", content);

            var responseString = await response.Content.ReadAsStringAsync();
            return true;
        }

        public async Task<bool> sendWithDraw(Events type, String command, Int32 data)
        {
            try
            {
                String message = JsonConvert.SerializeObject(new Models.Message(type, command, "", data), Formatting.Indented);

                var values = new Dictionary<string, string>
            {
               { "withdraw", message }
            };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("http://symetry.cloud/sam/ws/withdraw", content);

                var responseString = await response.Content.ReadAsStringAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
