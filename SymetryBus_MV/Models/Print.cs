﻿using PrintTicket;
using RBG200;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SymetryBus_MV.Models
{
    class Print
    {
        public static DateTime horaLAJM = DateTime.Now;

        public static bool printTicket(string claveRastreo, int tpoPrint, string referencia, string cuenta, string cr, string letra)
        {
            try
            {
                double total = 0;
                foreach (lstDenominaciones l_Detalle in GlyGlobals.infoDenominaciones)
                {
                    total = total + (l_Detalle.Cantidad * l_Detalle.Denominacion);
                }
                Random r = new Random();
                ModulePrint imprimir = new ModulePrint();
                imprimir.HeaderImage = Image.FromFile(Properties.Settings.Default.ImagenTicketH);
                imprimir.AddHeaderLine("");
                imprimir.AddHeaderLine("");
                imprimir.AddHeaderLine("                ");

                imprimir.AddHeaderLine("Estado:         " + "Cd México");
                imprimir.AddHeaderLine("Ciudad:         " + "Miguel Hidalgo");
                imprimir.AddHeaderLine("Sucursal:      " + "Sierra Candela");
                imprimir.AddHeaderLine("Fecha:         " + horaLAJM.ToString("yyyy-MM-dd"));
                imprimir.AddHeaderLine("Hora:         " + horaLAJM.ToString("HH:mm:ss"));
                imprimir.AddHeaderLine("Id Cajero:         " + "B18X003289");
                imprimir.AddHeaderLine("CR:         " + cr);
                imprimir.AddHeaderLine("#Cuenta:         " + "******" + cuenta.Substring(6));
                imprimir.AddHeaderLine("#Secuencia:        " + r.Next(00000, 99999));
                imprimir.AddHeaderLine("#Referencia:       " + referencia);
                if (tpoPrint == 1)
                {
                    imprimir.AddHeaderLine("Movimiento:       DEPOSITO DE EFECTIVO");
                }
                else
                {
                    imprimir.AddHeaderLine("Movimiento:       RETIRO DE EFECTIVO");
                }
                imprimir.AddHeaderLine("Divisa:                       " + "MXP");
                imprimir.AddHeaderLine("Importe:         " + total.ToString("$#,###,###,##0.00"));


                if (string.IsNullOrEmpty(claveRastreo))
                {

                    imprimir.BodyImage = Image.FromFile(Properties.Settings.Default.ImagenTicketNA);
                }
                else
                {
                    imprimir.AddHeaderLine("Importe Letra:         " + letra);
                    imprimir.AddHeaderLine("Clave de Rastreo:       " + claveRastreo);
                }
                imprimir.AddHeaderLine("");
                imprimir.AddHeaderLine("");
                foreach (lstDenominaciones l_Detalle in GlyGlobals.infoDenominaciones)
                {
                    try
                    {

                        imprimir.AddItem(l_Detalle.Cantidad.ToString(), l_Detalle.Item.ToString("C"),
                            l_Detalle.Denominacion.ToString());
                    }
                    catch (Exception p)
                    {
                        //MessageBox.Show(p.Message);
                    }
                }


                //imprimir.AddFooterLine("BILLETES TOTALES:           " +  GlyGlobals.infoDenominaciones.Sum(lstDenominaciones => lstDenominaciones..Cantidad).ToString("$#,###,###,##0.00"));
                //imprimir.AddFooterLine("MONEDAS TOTALES:           " + GlyGlobals.TotalMonedas.ToString("$#,###,###,##0.00"));
                if (tpoPrint == 1)
                {
                    imprimir.AddFooterLine("Total Depósito: " + total.ToString("$#,###,###,##0.00"));
                }
                else
                {
                    imprimir.AddFooterLine("Total de Retiro: " + total.ToString("$#,###,###,##0.00"));
                }

                imprimir.FooterImage = Image.FromFile(Properties.Settings.Default.ImagenTicketF);
                imprimir.PrintTicket(Properties.Settings.Default.Impresora, "Impresion MV");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
