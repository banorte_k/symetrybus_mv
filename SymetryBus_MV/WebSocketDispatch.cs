﻿using hcr360_api;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RBG200;
using SymetryBus_MV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utils;
using vtortola.WebSockets;

namespace SymetryBus_MV
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GlyDelegateCallback(IntPtr lpNotif);

    public class WebSocketDispatch
    {
        WebSocket ws;
        CancellationToken cancellation;
        public static GlyDelegateCallback NotifLibGly;
        public static GlyDelegateCallback NotifLibGlyDispense;
         MethodPublic @public = new MethodPublic();
        static bool idDeposit = false;
        public static float totalRetiro;

        public WebSocketDispatch(WebSocket ws, CancellationToken cancellation)
        {
            try
            {
                this.ws = ws;
                this.cancellation = cancellation;

                if (checkVigencia())
                {
                    HandleConnectionAsync();
                }
                else
                {
                    Send(Events.Success, "CONNECT", "Por favor comuniquese con Symetry Error GENERICO --S1968--");
                }
            }
            catch (Exception e)
            {
                Send(Events.Error, "ERROR", e.Message);
            }
        }

        private static bool checkVigencia()
        {
            DateTime l_hoy = DateTime.Now;
            DateTime l_vigencia = new DateTime(2029, 8, 30);
            return (l_hoy < l_vigencia);
        }

        async Task HandleConnectionAsync()
        {

            bool exit = false;
            String data = null;

            try
            {
                while (true)
                {
                    data = await ws.ReadStringAsync(cancellation).ConfigureAwait(false);
                    exit = ProcessCommand(data);
                }


                Console.WriteLine("Salto");

            }
            catch (Exception ex)
            {
                Send(Events.Error, "ERROR", ex.Message);
                closeConnection();
            }
        }

        private bool ProcessCommand(String data)
        {
            try
            {
                //Thread.Sleep(2000);
                var command = data.TrimEnd('\n').TrimEnd('\r').Split('-');
                switch (command[0])
                {
                    case "START":
                        switch (command[1])
                        {
                            case "OPEN":
                                int retOpen = 0;
                                switch (command[2])
                                {
                                    case "DEPOSIT":
                                        NotifLibGly = new GlyDelegateCallback(fnNotification);
                                        retOpen = RBG200.GlyDllApi.GLY_Open("RBG200", ref GlyGlobals.glyHandle);
                                        int retSetCallback = GlyDllApi.GLY_SetCallBack(GlyGlobals.glyHandle, NotifLibGly);
                                        if (retOpen == GlyCode.GLY_SUCCESS && retSetCallback == GlyCode.GLY_SUCCESS)
                                        {
                                            Send(Events.Success, command[0], "Open GlyApi OK Success.");
                                        }
                                        else
                                        {
                                            Send(Events.Error, command[0], "Wrong command, see manual for more info.");
                                        }
                                        break;
                                    case "RETIRO":
                                        NotifLibGlyDispense = new GlyDelegateCallback(fnNotificationDispense);
                                        retOpen = RBG200.GlyDllApi.GLY_Open("RBG200", ref GlyGlobals.glyHandle);
                                        int retSetCallbackDispense = GlyDllApi.GLY_SetCallBack(GlyGlobals.glyHandle, NotifLibGlyDispense);
                                        if (retOpen == GlyCode.GLY_SUCCESS && retSetCallbackDispense == GlyCode.GLY_SUCCESS)
                                        {
                                            Send(Events.Success, command[0], "Open GlyApi OK Success.");
                                        }
                                        else
                                        {
                                            Send(Events.Error, command[0], "Wrong command, see manual for more info.");
                                        }
                                        break;
                                }                              
                                
                                break;
                            case "CARD_DETECTED":
                                int ReturnCode;
                                bool CardDetected = false;
                                byte[] DatosRecibidos = new byte[255];
                                byte[] SW = new byte[255];
                                ReturnCode = hcr360Api.AbrirPuerto(1);
                               
                                //ReturnCode = hcr360Api.LeerEstatus(1, DatosRecibidos);
                                //Console.WriteLine(ReturnCode);
                                //Console.ReadLine();
                                int count = 0;
                                do
                                {
                                    DatosRecibidos = new byte[255];
                                    ReturnCode = hcr360Api.usi2ArmtoRead(1);
                                    Thread.Sleep(1000);
                                    ReturnCode = hcr360Api.ReadMagData(1, 1, DatosRecibidos);
                                    //ws.WriteString(count.ToString());
                                    if (DatosRecibidos[1].ToString() != "0")
                                    ////if (!System.Text.Encoding.UTF8.GetString(DatosRecibidos).Contains("!")|| !System.Text.Encoding.UTF8.GetString(DatosRecibidos).Contains("\0\0\0\0\0")||!System.Text.Encoding.UTF8.GetString(DatosRecibidos).Contains("*"))
                                    {
                                        Send(Events.Success, command[0], "CARD_DETECTED");
                                        CardDetected = true;
                                    }
                                    count++;
                                } while (CardDetected == false);
                                //Send(Events.Success, command[0], "CARD_DETECTED");
                                break;
                        }
                        break;
                    case "PROCESS":
                        switch (command[1])
                        {
                            case "STATUS_ADMON":
                                break;
                            case "STATUS":
                                IntPtr lpdwStatus = new IntPtr();
                                //if (GlyGlobals.glyHandle == IntPtr.Zero)
                                //{
                                NotifLibGly = new GlyDelegateCallback(fnNotification);
                                //Envio do comando Open
                                int retOpen = GlyDllApi.GLY_Open("RBG200", ref GlyGlobals.glyHandle);

                                //Envio do comando SetCallback passando a função de retorno
                                int retSetCallback = GlyDllApi.GLY_SetCallBack(GlyGlobals.glyHandle, NotifLibGly);

                                //}
                                int retGetStatus = GlyDllApi.GLY_GetStatus(GlyGlobals.glyHandle, ref lpdwStatus);
                                Thread.Sleep(1000);
                                GLYRBG200UNITSTATUS arrayStatus = new GLYRBG200UNITSTATUS();
                                GlyDllApi.GLY_RBG200_GetUnitStatus(GlyGlobals.glyHandle, ref arrayStatus);
                                //arrayStatus
                                //Random error = new Random();
                                //arrayStatus.ErrorCode = error.Next(0,9999);
                                //ws.WriteString("Status LAJM");
                                Send(Events.Error, "GLY_STATUS_ERROR", arrayStatus);
                                GlyDllApi.GLY_Close(GlyGlobals.glyHandle);
                                break;
                            case "LOG":
                                Log.Write("FrontClient", command[2], command[3]);
                                Send(Events.Success, "LOG", "Se guardo correctamente.");
                                break;
                            case "SETUP":
                                Task<JObject> RESULY = @public.GetSetup(command[2]);
                                Send(Events.Success, "SETUP", RESULY);
                                break;
                        }
                        break;
                    case "COMMAND":
                        switch (command[1])
                        {
                            case "VALIDACUENTA":
                                string mensaje = string.Empty;
                                string nombre = string.Empty;
                                string monto = string.Empty;
                                GSI_API.Operation.LoginGsi(command[2], out mensaje, out nombre, out monto);
                                List<string> datos = new List<string>();
                                datos.Add(mensaje);
                                datos.Add(nombre);
                                datos.Add(monto);
                                Send(Events.Success, command[1], datos);
                                break;
                            case "DEPOSIT":
                                idDeposit = true;
                                IntPtr glyRequestId = new IntPtr();                              
                                string[] part = command[2].Split(',');
                                GlyGlobals.LstNotes = new List<lstNote>();
                                foreach (string a in part)
                                {
                                    GlyGlobals.LstNotes.Add(new lstNote(a, "0"));
                                }                                                               
                                int retDeposit = GlyDllApi.GLY_AsyncAutoRepetitiveDeposit(GlyGlobals.glyHandle, NotifLibGly, ref glyRequestId);
                                break;
                            case "ENDDEPOSIT":
                                IntPtr glyRequestIdEnd = new IntPtr();
                                IntPtr glyRequestIdInterrupt = new IntPtr();
                                Thread.Sleep(2000);
                                if (GlyDllApi.GLY_AsyncStore(GlyGlobals.glyHandle, NotifLibGly, ref glyRequestIdEnd) == 0)
                                {
                                    Thread.Sleep(2000);
                                    if (GlyDllApi.GLY_AsyncCancelInterruptDeposit(GlyGlobals.glyHandle, NotifLibGly, ref glyRequestIdInterrupt) == 0)
                                    {
                                        GlyDllApi.GLY_Close(GlyGlobals.glyHandle);
                                        Send(Events.Success, command[0], "End Deposit GlyApi OK Success.");
                                        GSI_API.Operation send = new GSI_API.Operation();
                                        string clave = string.Empty;
                                        string cr = string.Empty;
                                        string letra = string.Empty;
                                        if (send.DepositoGsi(GlyGlobals.LstNotes, command[3], command[2], out clave, out cr, out letra))
                                        {
                                            @public.sendTrans(Events.Success, command[0], GlyGlobals.LstNotes);
                                            Models.Print.printTicket(clave, 1, command[2], command[3], cr, letra);

                                        }
                                        else
                                        {
                                            @public.sendTrans(Events.Success, command[0], GlyGlobals.LstNotes);
                                            Models.Print.printTicket(clave, 1, command[2], command[3], cr, letra);
                                        }

                                    }

                                }
                                else
                                {
                                    Send(Events.Error, command[0], "Wrong command, see manual for more info.");
                                }
                                break;
                            case "CANCELDEPOSIT":
                                break;
                            case "END_OUT_CASH":
                                IntPtr glyRequestIdPresent = new IntPtr();
                                GlyDllApi.GLY_AsyncPresent(GlyGlobals.glyHandle, NotifLibGlyDispense, ref glyRequestIdPresent);
                                //Thread.Sleep(1000);
                                GlyDllApi.GLY_Close(GlyGlobals.glyHandle);
                                Models.Print.printTicket(DateTime.Now.Ticks.ToString(), 0, DateTime.Now.Ticks.ToString(), DateTime.Now.Ticks.ToString(), "12345", string.Empty);
                                break;
                            case "OUT_CASH":
                                idDeposit = false;
                                //Thread.Sleep(2000);
                                totalRetiro = float.Parse(command[2].Replace('$', ' ').Trim());
                              
                                System.Diagnostics.Process process = new System.Diagnostics.Process();
                                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                                startInfo.UseShellExecute = true;
                                startInfo.Verb = "runas";
                                startInfo.WorkingDirectory = Environment.CurrentDirectory;

                                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                startInfo.FileName = Environment.ExpandEnvironmentVariables("%SystemRoot%") + @"\System32\cmd.exe";
                                startInfo.Arguments = "/ user:Administrator \"cmd /K ConsoleRGB200.exe "+ command[2].Replace('$', ' ').Trim();
                                Console.WriteLine(startInfo.Arguments.ToString());

                                process.StartInfo = startInfo;                              

                                if (process.Start())

                                {
                                    ws.WriteString("OK");
                                    Send(Events.Success, "OK", "OK");
                                    try
                                    {
                                        @public.sendWithDraw(Events.Success, "OK", Convert.ToInt32(command[2]));
                                    }
                                    catch
                                    {

                                    }

                                }
                                else
                                {
                                    Send(Events.Error, "0", "0");
                                }
                                break;
                        }
                        break;
                    case "EXIT":
                        
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                Send(Events.Error, "ERROR", ex.Message);
                return false;
            }
        }

        private void closeConnection()
        {
            try
            {
                ws.Close();
                ws.Dispose();
            }
            catch (Exception ex)
            {

            }
        }
        #region Send
        private void Send(Events type, String command, GLYRBG200UNITSTATUS data)
        {
            try
            {
                String message = JsonConvert.SerializeObject(new Message(type, command, data), Formatting.Indented);
                if (ws.IsConnected)
                {
                    Log.Output(message);
                    ws.WriteString(message);
                }
            }
            catch (Exception e)
            {
                Send(Events.Error, "ERROR", e.Message);
                Log.Output(e.Message);
            }

        }
        private void Send(Events type, String command, List<string> data)
        {
            try
            {

                String message = JsonConvert.SerializeObject(new Message(type, command, data), Formatting.Indented);
                if (ws.IsConnected)
                {
                    Log.Output(message);
                    ws.WriteString(message);
                }
            }
            catch (Exception e)
            {
                Send(Events.Error, "ERROR", e.Message);
                Log.Output(e.Message);
            }

        }

        private void Send(Events type, String command, List<lstNote> data)
        {
            try
            {
                String message = JsonConvert.SerializeObject(new Message(type, command, data), Formatting.Indented);
                if (ws.IsConnected)
                {
                    Log.Output(message);
                    ws.WriteString(message);
                }
            }
            catch (Exception e)
            {
                Send(Events.Error, "ERROR", e.Message);
                Log.Output(e.Message);
            }

        }

        public void Send(Events type, String command, String data)
        {
            try
            {
                String message = JsonConvert.SerializeObject(new Message(type, command, data, 0), Formatting.Indented);
                if (ws.IsConnected)
                {
                    Log.Output(message);
                    ws.WriteString(message);
                }
            }
            catch (Exception ex)
            {
                Send(Events.Error, "ERROR", ex.Message);
                Log.Output(ex.Message);
            }

        }

        private void Send(Events type, String command, Task<JObject> data)
        {
            try
            {

                String message = JsonConvert.SerializeObject(new Message(type, command, data), Formatting.Indented);
                if (ws.IsConnected)
                {
                    Log.Output(message);
                    ws.WriteString(message);
                }
            }
            catch (Exception e)
            {
                Send(Events.Error, "ERROR", e.Message);
                Log.Output(e.Message);
            }

        }
        #endregion

        private void fnNotificationDispense(IntPtr lpNotif)
        {
            try
            {

            }
            catch
            {

            }
        }

        private void fnNotification(IntPtr lpNotif)
        {
            if (idDeposit)
            {
                RBG200.GLYPARAM param = new RBG200.GLYPARAM();

                try
                {
                    param = (RBG200.GLYPARAM)Marshal.PtrToStructure(lpNotif, typeof(RBG200.GLYPARAM));

                    switch (param.messageId)
                    {
                        //Tratamento para retorno de contagem
                        case RBG200.GlyCode.GLY_DEPOSIT_COUNT_CHANGE:
                            RBG200.GLYCOUNTRESULT countResult = (RBG200.GLYCOUNTRESULT)Marshal.PtrToStructure(param.buffer, typeof(RBG200.GLYCOUNTRESULT));
                            RBG200.GlyGlobals.infoDenominaciones = new List<RBG200.lstDenominaciones>();

                            for (int indexDenomination = 0; indexDenomination < countResult.C4.arraySize; indexDenomination++)
                            {
                                IntPtr ptrCurrency = new IntPtr(countResult.C4.currencies.ToInt32() + indexDenomination * Marshal.SizeOf(typeof(GLYCURRENCY)));

                                //Estrutura com valores contados
                                RBG200.GLYCURRENCY glyCurrency = (RBG200.GLYCURRENCY)Marshal.PtrToStructure(ptrCurrency, typeof(RBG200.GLYCURRENCY));
                                switch (glyCurrency.value)
                                {
                                    case 20:
                                        if (glyCurrency.counts != 0)
                                        {
                                            GlyGlobals.infoDenominaciones.Add(new lstDenominaciones(20, glyCurrency.counts, 20));
                                            GlyGlobals.LstNotes[0].Count = glyCurrency.counts.ToString();
                                        }
                                        break;
                                    case 50:
                                        if (glyCurrency.counts != 0)
                                        {
                                            GlyGlobals.infoDenominaciones.Add(new lstDenominaciones(50, glyCurrency.counts, 50));
                                            GlyGlobals.LstNotes[1].Count = glyCurrency.counts.ToString();
                                        }
                                        break;
                                    case 100:
                                        if (glyCurrency.counts != 0)
                                        {
                                            GlyGlobals.infoDenominaciones.Add(new lstDenominaciones(100, glyCurrency.counts, 100));
                                            GlyGlobals.LstNotes[2].Count = glyCurrency.counts.ToString();
                                        }
                                        break;
                                    case 200:
                                        if (glyCurrency.counts != 0)
                                        {
                                            GlyGlobals.infoDenominaciones.Add(new lstDenominaciones(200, glyCurrency.counts, 200));
                                            GlyGlobals.LstNotes[3].Count = glyCurrency.counts.ToString();
                                        }
                                        break;
                                    case 500:
                                        if (glyCurrency.counts != 0)
                                        {
                                            GlyGlobals.infoDenominaciones.Add(new lstDenominaciones(500, glyCurrency.counts, 500));
                                            GlyGlobals.LstNotes[4].Count = glyCurrency.counts.ToString();
                                        }
                                        break;
                                    case 1000:
                                        if (glyCurrency.counts != 0)
                                        {
                                            GlyGlobals.infoDenominaciones.Add(new lstDenominaciones(1000, glyCurrency.counts, 1000));
                                            GlyGlobals.LstNotes[5].Count = glyCurrency.counts.ToString();
                                        }
                                        break;
                                }
                            }
                            ws.WriteString("Status LAJM");
                            Send(Events.Success, "fnNotification", GlyGlobals.LstNotes);
                            break;
                        case 2162716:
                            IntPtr glyRequestId = new IntPtr();
                            int retDeposit = GlyDllApi.GLY_AsyncAutoRepetitiveDeposit(GlyGlobals.glyHandle, NotifLibGly, ref glyRequestId);
                            break;
                    }


                }
                catch (Exception ex)
                {
                    Send(Events.Error, "fnNotification", ex.Message);
                }
            }
        }

        private void validaRetiro()
        {
            long countNote = (long)0;
            try
            {
                foreach (lstNote a in GlyGlobals.LstNotes)
                {
                    string note = a.Note;
                    if (note == "20")
                    {
                        countNote += (long)(Convert.ToInt32(a.Count) * Convert.ToInt32(a.Note));
                    }
                    else if (note == "50")
                    {
                        countNote += (long)(Convert.ToInt32(a.Count) * Convert.ToInt32(a.Note));
                    }
                    else if (note == "100")
                    {
                        countNote += (long)(Convert.ToInt32(a.Count) * Convert.ToInt32(a.Note));
                    }
                    else if (note == "200")
                    {
                        countNote += (long)(Convert.ToInt32(a.Count) * Convert.ToInt32(a.Note));
                    }
                    else if (note == "500")
                    {
                        countNote += (long)(Convert.ToInt32(a.Count) * Convert.ToInt32(a.Note));
                    }
                    else if (note == "1000")
                    {
                        countNote += (long)(Convert.ToInt32(a.Count) * Convert.ToInt32(a.Note));
                    }
                }
                if (countNote == (long)0)
                {
                    this.ws.WriteString("0");
                    this.Send(Events.Error, "0", "0");
                }
                else if ((float)countNote >= WebSocketDispatch.totalRetiro)
                {
                    this.Send(Events.Success, "OK", "OK");
                    Threading send = new Threading();
                }
                else
                {
                    this.ws.WriteString("0");
                    this.Send(Events.Error, "0", "0");
                }
            }
            catch (Exception exception)
            {
            }
        }

    }
}
