﻿using System;
using System.Threading.Tasks;
using System.Net;
using vtortola.WebSockets;
using System.Threading;
using Utils;
using System.IO;
using vtortola.WebSockets.Deflate;
//using vtortola.WebSockets.Rfc6455;

namespace SymetryBus_MV
{
    class WebSocketServer
    {
        WebSocketListener server;
        CancellationTokenSource cancellation = new CancellationTokenSource();

        public WebSocketServer()
        {
            try
            {
                server = new WebSocketListener(new IPEndPoint(IPAddress.Parse("192.168.2.6"), 6969));
                
                server.Standards.RegisterStandard(new WebSocketFactoryRfc6455());
            }
            catch(Exception ex)
            {

            }
        }


        public void StartServer()
        {
            try
            {
                server.StartAsync();//.AcceptWebSocketAsync(cancellation.Token);//.StartAsync();
                Log.Write("Main", "TryIcon", "Servicio iniciado");
                //Utils.NotifyEvent.MessageEvent("Servicio web socket iniciado...");
                Log.Output("Servicio websocket iniciado...");
                cancellation.CancelAfter(300000);
                var task = Task.Run(() => AcceptWebSocketClientsAsync(server, cancellation.Token));
            }
            catch (Exception e)
            {
                Log.Write("WebSocketServer", "StartServer()", e.Message);
                Log.Output("Error al iniciar servicio");
            }
        }

        public void StopServer()
        {
            server.Stop();
            server.Dispose();
        }

        static async Task AcceptWebSocketClientsAsync(WebSocketListener server, CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    WebSocket ws = await server.AcceptWebSocketAsync(token).ConfigureAwait(false);
                    if (ws != null)
                    {
                        //Utils.NotifyEvent.MessageEvent("Petición aceptada...");
                        Log.Output("Petición aceptada de " + ws.RemoteEndpoint.Address.ToString());
                        
                        await Task.Run(() => new WebSocketDispatch(ws, token));
                    }
                }
                catch (Exception aex)
                {

                }
            }
        }

    }
}
