﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;
using static Utils.Utils;

namespace SymetryBus_MV
{
    public class TryIcon : ApplicationContext
    {

        public NotifyIcon TrayIcon;
        public ContextMenu ContextMenu { get; private set; }
        public bool Visible { get; private set; }
        public Monitor monitor;
        public MenuItem menuNotify;

        private WebSocketServer ws;

        public TryIcon()
        {
            menuNotify = new MenuItem("Notificaciones activas", Notify);

            TrayIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.Icon,
                ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Acerca de ...", About),
                    new MenuItem("Monitor", Monitor),
                    menuNotify,
                    new MenuItem("Salir", Exit),
                }),

                Visible = true
            };

            monitor = new Monitor();
            monitor.Hide();

            NotifyEvent.MessageEvent = Message;

            Application.ApplicationExit += Application_ApplicationExit;
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;

            Log.Write("Main", "TryIcon", "Iniciando servicio");
            ws = new WebSocketServer();
            ws.StartServer();           
        }

        private void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            TrayIcon.Visible = false;
            TrayIcon.Dispose();
        }

        private void TryIcon_ThreadExit(object sender, EventArgs e)
        {
            TrayIcon.Visible = false;
            TrayIcon.Dispose();
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            TrayIcon.Visible = false;
            TrayIcon.Dispose();
        }

        public void Message(string message)
        {
            if (notify)
            {
                TrayIcon.BalloonTipTitle = "Symetry BUS";
                TrayIcon.BalloonTipText = message;
                TrayIcon.ShowBalloonTip(5000);
            }
        }

        private void About(object sender, EventArgs e)
        {
            MessageBox.Show("Symetry BUS V.1.0");
        }

        private void Exit(object sender, EventArgs e)
        {
            ws.StopServer();
            Application.Exit();
            TrayIcon.Visible = false;
            TrayIcon.Dispose();
        }

        private void Monitor(object sender, EventArgs e)
        {
            if (monitor.Visible)
            {
                monitor.Hide();
            }
            else
            {
                monitor.Show();
                monitor.Refresh();
            }
        }

        private void Notify(object sender, EventArgs e)
        {
            if (notify)
            {
                notify = false;
                menuNotify.Text = "Notificaciones desactivadas";
            }
            else
            {
                notify = true;
                menuNotify.Text = "Notificaciones activas";
            }
        }

    }
}
