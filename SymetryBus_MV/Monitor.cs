﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utils;

namespace SymetryBus_MV
{
    public partial class Monitor : Form
    {
        public Monitor()
        {
            InitializeComponent();
            Log.text = this.textBoxOutput;
        }

        private void AddTextOutput(string text)
        {
            textBoxOutput.AppendText(text + "\r\n");
        }

        private void textBoxOutput_TextChanged(object sender, EventArgs e)
        {

        }

        private void Monitor_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void Monitor_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}
