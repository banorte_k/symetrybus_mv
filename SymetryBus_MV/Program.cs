﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SymetryBus_MV
{
    class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            bool flag = false;
            Mutex mutex = null;
            try
            {
                mutex = new Mutex(true, "BUS_MV.Singleton");
                flag = mutex.WaitOne(1, false);
            }
            catch (AbandonedMutexException)
            {
                flag = true;
            }
            if (flag)
            {
                try
                {
                    Application.Run(new TryIcon());
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }
        }
    }
}
