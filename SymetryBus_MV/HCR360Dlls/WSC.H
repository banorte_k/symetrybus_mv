/*
**  wsc.h
**
**  Use for both Win16 & Win32.
*/
#ifndef __WSC_H__
#define __WSC_H__


 int  SioBaud(int,unsigned);
 int  SioBrkSig(int,char);
 int  SioCTS(int);
 int  SioDCD(int);
 int  SioDebug(int);
 int  SioDone(int);
 int  SioDSR(int);
 int  SioDTR(int,char);
 int  SioFlow(int,char);
 int  SioInfo(char);
 int  SioGetc(int);
 int  SioGets(int,LPSTR,unsigned);
 int  SioParms(int,int,int,int);
 int  SioPutc(int,char);
 int  SioPuts(int,LPSTR,unsigned);
 int  SioRead(int,int);
 int  SioReset(int,int,int);
 int  SioRI(int);
 int  SioRTS(int,char);
 int  SioRxClear(int);
 int  SioRxQue(int);
 int  SioStatus(int,unsigned);
 int  SioTxClear(int);
 int  SioTxQue(int);
 int  SioUnGetc(int,char);
 int  Pack(LPSTR,int);
 int  Pack_Dat(LPSTR,unsigned char *,int);
 int  Unpack(LPSTR,int);
 int  Des(int,LPSTR,LPSTR,LPSTR);
 int  Xorf(LPSTR,LPSTR,LPSTR);
 int  Pmac(LPSTR,LPSTR,LPSTR,int,LPSTR);
 int  Lrcf(LPSTR,int);
 int  FreeSR(int,LPSTR,int,LPSTR);
 int  FreeSRD(int,LPSTR,int,LPSTR);
 int  Free800(int,LPSTR,int,LPSTR);
 int  Free900(int,LPSTR,int,LPSTR);
 int  Pmac1(LPSTR,LPSTR,LPSTR,int,LPSTR);
 int  Pmac2(LPSTR,LPSTR,LPSTR,int,LPSTR);
 int  Pmac3(LPSTR,LPSTR,LPSTR,int,LPSTR);
 int  Fmac(LPSTR,LPSTR,LPSTR,int,LPSTR);
 int  Reset_Card(unsigned char *,unsigned char *);
 int  Eject_Card(unsigned char *);
 int  Select_DF(unsigned char *,unsigned char *);
 int  Select_EF(unsigned char *,unsigned char *);
 int  Read_Rec(LPSTR,unsigned char *,unsigned char *);
 int  Write_Record(int,LPSTR,unsigned char *);
 int  Write_R_TAC(int,LPSTR,unsigned char *,unsigned char *);
 int  Bin_ADD(LPSTR,unsigned char *);
 int  Update_Rec(int,LPSTR,unsigned char *);
 int  Create_File(LPSTR,unsigned char *);
 int  Tml_Authen(LPSTR,unsigned char *);
 int  Read_Bin(unsigned char *,unsigned char *);
 int  C_Gen_Ran(unsigned char *,unsigned char *);
 int  Verify_PIN(unsigned char *);
 int  Verify_PIN1(unsigned char *);
 int  Select_MF(unsigned char *);
 int  Chg_MK(LPSTR,unsigned char *);
 int  S_Gen_Ran(unsigned char *,unsigned char *);
 int  Write_Key(int,LPSTR,unsigned char *);
 int  Read_Key(LPSTR,unsigned char *,unsigned char *);
 int  Erase_EF(unsigned char *);
 int  Unlock_Key(LPSTR,unsigned char *);
 int  Read_SMNO(unsigned char *,unsigned char *);
 int  Read_MK(unsigned char *,unsigned char *);
 int  Select_ADF(LPSTR,unsigned char *);
 int  Select_AEF(LPSTR,unsigned char *);
 int  Select_HMF(unsigned char *, unsigned char *);
 int  Select_HDF(LPSTR,unsigned char *,unsigned char *);
 int  Select_HEF(LPSTR,unsigned char *,unsigned char *);
 int  Verify_TSC(LPSTR,unsigned char *);
 int  Read_All(LPSTR,unsigned char *,unsigned char *);
 int  adjparity(LPSTR ,LPSTR);
 int  Session_Auth(LPSTR ,LPSTR ,LPSTR);
#ifdef WIN32
 int  SioWinError(LPSTR,int);
#endif


#define COM1   0
#define COM2   1
#define COM3   2
#define COM4   3
#define COM5   4
#define COM6   5
#define COM7   6
#define COM8   7
#define COM9   8
#define COM10  9
#define COM11 10
#define COM12 11
#define COM13 12
#define COM14 13
#define COM15 14

/* Parity Codes */

#define NoParity 0
#define OddParity  1
#define EvenParity 2
#define MarkParity 3
#define SpaceParity 4

/* Stop Bit Codes */

#define OneStopBit  0
#define TwoStopBits 2

/* Word Length Codes */

#define WordLength5  5
#define WordLength6  6
#define WordLength7  7
#define WordLength8  8

/* return codes */

#define WSC_NO_DATA   (-100)
#define WSC_RANGE     (-101)
#define WSC_ABORTED   (-102)
#define WSC_WIN32ERR  (-103)
#define WSC_EXPIRED   (-104)
#define IE_BADID      (-1)
#define IE_OPEN       (-2)
#define IE_NOPEN      (-3)
#define IE_MEMORY     (-4)
#define IE_DEFAULT    (-5)
#define IE_HARDWARE   (-10)
#define IE_BYTESIZE   (-11)
#define IE_BAUDRATE   (-12)

/* baud codes */

#define Baud110    0
#define Baud300    1
#define Baud1200   2
#define Baud2400   3
#define Baud4800   4
#define Baud9600   5
#define Baud19200  6
#define Baud38400  7
#define Baud57600  8
#define Baud115200 9

/* SioGetError masks */

#define WSC_RXOVER   0x0001
#define WSC_OVERRUN  0x0002
#define WSC_PARITY   0x0004
#define WSC_FRAME    0x0008
#define WSC_BREAK    0x0010
#define WSC_TXFULL   0x0100

#endif