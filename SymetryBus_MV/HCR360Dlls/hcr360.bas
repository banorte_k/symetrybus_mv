Attribute VB_Name = "HCR360"
'================================================================
'File Name: HCR360.bas
'Version  : 1A
'================================================================
Option Explicit

Public Declare Function OpenCom Lib "hcr360.dll" (ByVal port As Long, ByVal baud As Long) As Long
Public Declare Sub CloseCom Lib "hcr360.dll" (ByVal port As Long)

'Usi2
Public Declare Sub usi2SendData Lib "hcr360.dll" (ByVal port As Long, _
             ByVal Cmd As Byte, ByVal DataLen As Integer, DataBuf As Byte)
Public Declare Function usi2ReceiveData Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long

'//public declare function CardMoveNotify(int Port);
'Public Declare Function usiNotifyData Lib "HCR360.DLL" (ByVal Port As Long, RcvData As Byte) As Long

Public Declare Function usi2Exchange Lib "hcr360.dll" (ByVal port As Long, _
             ByVal Cmd As Byte, ByVal SndDataLen As Integer, SndData As Byte, RcvData As Byte) As Long
'Public Declare Function UsiGetErrCode Lib "HCR360.DLL" () As Byte

'Public Declare Function usiGetReaderVersion Lib "HCR360.DLL" (ByVal Port As Long, ByVal MaskCode As String) As Long
'Public Declare Function GetReaderMaskCode Lib "HCR360.DLL" (ByVal Port As Long, _
'             ByVal MaskCode As String) As Long

Public Declare Function usi2CheckCardPresence Lib "hcr360.dll" (ByVal port As Long) As Long
Public Declare Function usi2CardMoveNotify Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long

Public Declare Function usi2Retransmit Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2ReaderVerReport Lib "hcr360.dll" (ByVal port As Long, ByVal SubCmd As Byte, _
            RcvData As Byte) As Long
Public Declare Function usi2SwitchStatus Lib "hcr360.dll" (ByVal port As Long) As Long
Public Declare Function usi2SetVerboseMode Lib "hcr360.dll" (ByVal port As Long) As Long
Public Declare Function usi2ReaderStatus Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2SetGreenLed Lib "hcr360.dll" (ByVal port As Long, ByVal LedStatus As Byte) As Long
Public Declare Function usi2SetRedLed Lib "hcr360.dll" (ByVal port As Long, ByVal LedStatus As Byte) As Long
Public Declare Function usi2Latch Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2Unlatch Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2ConfigReport Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2WarmReset Lib "hcr360.dll" (ByVal port As Long) As Long

'Reader and CPU card

    '//IC card
Public Declare Function usi2CardPowerUp Lib "hcr360.dll" (ByVal port As Long, _
              Vcc As Byte, ByVal EmvComply As Byte, Atr As Byte) As Long
Public Declare Function usi2CardPowerOff Lib "hcr360.dll" (ByVal port As Long) As Long
Public Declare Function usi2SendApdu Lib "hcr360.dll" (ByVal port As Long, ByVal ApduLen As Integer, _
             ApduCmd As Byte, RcvData As Byte, SW As Byte) As Long
Public Declare Function usi2SelectIccConnector Lib "hcr360.dll" (ByVal port As Long, ByVal IccConnector As Byte) As Long

    '//Memory card
Public Declare Function usi2SelectMemCardType Lib "hcr360.dll" (ByVal port As Long, ByVal MemCardType As Byte, ByVal PageSize As Byte) As Long
Public Declare Function usi2SendMemCardApdu Lib "hcr360.dll" (ByVal port As Long, ByVal ApduLen As Integer, ApduCmd As Byte, _
                                                   RcvData As Byte, SW As Byte) As Long
Public Declare Function usi2ReportMemCardType Lib "hcr360.dll" (ByVal port As Long) As Long

Public Declare Function usi2MemoryVerify Lib "hcr360.dll" (ByVal port As Long, ByVal Nums As Integer, PassWord As Byte, SW As Byte) As Long

Public Declare Function usi2MemoryAuthenticate Lib "hcr360.dll" (ByVal port As Long, ByVal Nums As Integer, _
                                                                      DataBuf As Byte, SW As Byte) As Long
Public Declare Function usi2MemoryExtAuthenticate Lib "hcr360.dll" (ByVal port As Long, ByVal Nums As Integer, _
                                                                      DataBuf As Byte, SW As Byte) As Long
Public Declare Function usi2MemoryReadData Lib "hcr360.dll" (ByVal port As Long, ByVal Addr As Integer, ByVal Nums As Integer, _
                                                                 RcvData As Byte, SW As Byte) As Long
Public Declare Function usi2MemoryWriteData Lib "hcr360.dll" (ByVal port As Long, ByVal Addr As Integer, ByVal Nums As Integer, _
                                                                 DataBuf As Byte, SW As Byte) As Long
Public Declare Function usi2MemoryEraseData Lib "hcr360.dll" (ByVal port As Long, ByVal Addr As Integer, SW As Byte) As Long
Public Declare Function usi2MemoryRestoreData Lib "hcr360.dll" (ByVal port As Long, ByVal Addr As Integer, SW As Byte) As Long
Public Declare Function usi2MemoryWriteProtect Lib "hcr360.dll" (ByVal port As Long, ByVal Addr As Integer, ByVal Nums As Integer, _
                                                                 DataBuf As Byte, SW As Byte) As Long
Public Declare Function usi2MemoryReadProtect Lib "hcr360.dll" (ByVal port As Long, ByVal Addr As Integer, ByVal Nums As Integer, _
                                                                 RcvData As Byte, SW As Byte) As Long
Public Declare Function usi2MemoryEraseUserArea Lib "hcr360.dll" (ByVal port As Long, ByVal Nums As Integer, ErsPSC As Byte _
                                                                  , SW As Byte) As Long
Public Declare Function usi2MemoryChangePSC Lib "hcr360.dll" (ByVal port As Long, ByVal Nums As Integer, NewPSC As Byte _
                                                                  , SW As Byte) As Long
Public Declare Function usi2MemoryReadErrCount Lib "hcr360.dll" (ByVal port As Long, ErrCount As Byte, SW As Byte) As Long


    '//Mag card cmd
Public Declare Function usi2ArmtoRead Lib "hcr360.dll" (ByVal port As Long) As Long
Public Declare Function usi2Abort Lib "hcr360.dll" (ByVal port As Long) As Long
Public Declare Function usi2ReadMagData Lib "hcr360.dll" (ByVal port As Long, ByVal selectTK As Byte, RcvData As Byte) As Long
Public Declare Function usi2TK1Data Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2TK2Data Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2TK3Data Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte) As Long
Public Declare Function usi2ISOData Lib "hcr360.dll" (ByVal port As Long, ByVal Track As Byte, ByVal viaISO As Byte, RcvData As Byte) As Long
Public Declare Function usi2ForwardCusData Lib "hcr360.dll" (ByVal port As Long, ByVal Track As Byte, ByVal viaNull As Byte, _
                                                                 ByVal chrNums As Byte, RcvData As Byte) As Long
Public Declare Function usi2ReverseCusData Lib "hcr360.dll" (ByVal port As Long, ByVal Track As Byte, ByVal viaNull As Byte, _
                                                                 ByVal chrNums As Byte, RcvData As Byte) As Long


'TLP224Turbo Protocol
Public Declare Sub TLP224Turbo_SendData Lib "hcr360.dll" (ByVal port As Long, ByVal Cmd As Byte, ByVal DataLen As Integer, DataBuf As Byte)
Public Declare Function TLP224Turbo_ReceiveData Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte, ByVal lDelayTime As Long) As Long
Public Declare Function TLP224Turbo_Exchange Lib "hcr360.dll" (ByVal port As Long, ByVal Cmd As Byte, ByVal SndDataLen As Integer, SndData As Byte, RcvData As Byte, ByVal lDelayTime As Long) As Long

'TLP224 Protocol
Public Declare Sub TLP224_SendData Lib "hcr360.dll" (ByVal port As Long, ByVal Cmd As Byte, ByVal DataLen As Integer, DataBuf As Byte)
Public Declare Function TLP224_ReceiveData Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte, ByVal lDelayTime As Long) As Long
Public Declare Function TLP224_Exchange Lib "hcr360.dll" (ByVal port As Long, ByVal Cmd As Byte, ByVal SndDataLen As Integer, SndData As Byte, RcvData As Byte, ByVal lDelayTime As Long) As Long

'USI1 Protocol
Public Declare Sub USI1_SendData Lib "hcr360.dll" (ByVal port As Long, ByVal Cmd As Byte, ByVal DataLen As Integer, DataBuf As Byte)
Public Declare Function USI1_ReceiveData Lib "hcr360.dll" (ByVal port As Long, RcvData As Byte, ByVal lDelayTime As Long) As Long
Public Declare Function USI1 Lib "hcr360.dll" (ByVal port As Long, ByVal Cmd As Byte, ByVal SndDataLen As Integer, SndData As Byte, RcvData As Byte, ByVal lDelayTime As Long) As Long
  
             
' COM port codes
 Global Const COM1 = 0
 Global Const COM2 = 1
 Global Const COM3 = 2
 Global Const COM4 = 3
 Global Const COM5 = 4
 Global Const COM6 = 5
 Global Const COM7 = 6
 Global Const COM8 = 7
 Global Const COM9 = 8
 Global Const COM10 = 9
 Global Const COM11 = 10
 Global Const COM12 = 11
 Global Const COM13 = 12
 Global Const COM14 = 13
 Global Const COM15 = 14
 Global Const COM16 = 15

' Parity Codes
Global Const NoParity = 0
Global Const OddParity = 1
Global Const EvenParity = 2
Global Const MarkParity = 3
Global Const SpaceParity = 4

' Stop Bit Codes
Global Const OneStopBit = 0
Global Const TwoStopBits = 2

' Word Length Codes
Global Const WordLength5 = 5
Global Const WordLength6 = 6
Global Const WordLength7 = 7
Global Const WordLength8 = 8

' baud codes
Global Const Baud1200 = 2
Global Const Baud2400 = 3
Global Const Baud4800 = 4
Global Const Baud9600 = 5
Global Const Baud19200 = 6
Global Const Baud38400 = 7
Global Const Baud57600 = 8
Global Const Baud115200 = 9

'Public Const Usi2ACK& = &H60
'Public Const Usi2NACK& = &HE0

Rem Define IC Card VCC
Public Const V1_8 As Byte = 1
Public Const V3 As Byte = 3
Public Const V5 As Byte = 5

'---------------------------------------------------------------
' Genernal Purpose Functions
'---------------------------------------------------------------
' '1'2'3'4'5' -> "3132333435"
Public Function BinToHexStr(baInPut() As Byte, ByVal nLen As Integer) As String
    Dim buf As String
    Dim i As Integer
    Dim Value As Byte

    For i = 0 To (nLen - 1)
        Value = baInPut(i)
        If Value <= 15 Then
           buf = buf + "0" + Hex$(Value)
        Else
           buf = buf + Hex$(Value)
        End If
    Next i
    BinToHexStr$ = buf  'Modify by Steven
End Function

' "3132333435"-> '1'2'3'4'5'
Public Function HexStrToBin(strInput As String, baOutPut() As Byte) As Integer
    Dim i As Integer, j As Integer
    Dim nStrLen As Integer
    Dim buf As String

    nStrLen = Len(strInput)
       
    j = 0
    For i = 1 To nStrLen Step 2
        buf$ = "&H" + Mid$(strInput$, i, 2)
        baOutPut(j) = Val(buf$)
        j = j + 1
    Next i
    HexStrToBin% = nStrLen / 2
End Function

' '1'2'3'4'5' -> "12345"
Public Function BinAryToAsciiStr(baInPut() As Byte, ByVal nLen As Integer) As String
    Dim buf As String
    Dim i As Integer
    Dim Value As Byte

    For i = 0 To (nLen - 1)
        Value = baInPut(i)
        If Value <= 15 Then
           buf = buf + "<0" + Hex$(Value) + ">"
        ElseIf Value <= 31 Or Value >= 127 Then
           buf = buf + "<" + Hex$(Value) + ">"
        Else
           buf = buf + Chr(Value)
        End If
    Next i
    BinAryToAsciiStr = buf  'Modify by Jay
End Function

' "3132333435" -> "12345"
Public Function HexStrToAsciiStr(HexStr As String) As String
    Dim buf As String
    Dim i As Integer
    Dim Value As Byte

    For i = 1 To Len(HexStr) Step 2
        Value = Val("&H" + Mid(HexStr, i, 2))
        If Value <= 15 Then
           buf = buf & "<0" & Hex(Value) & ">"
        ElseIf Value <= 31 Or Value >= 127 Then
           buf = buf + "<" + Hex(Value) + ">"
        Else
           buf = buf + Chr(Value)
        End If
    Next i
    HexStrToAsciiStr = buf  'Modify by Jay
End Function
'---------------------------------------------------------------
' communication
'---------------------------------------------------------------
Public Function vbOpenCom(port As Long, baud As Long) As Integer
   vbOpenCom% = usiOpenCom(port, baud)
End Function

Public Sub vbCloseCom(port As Long)
    Call usiCloseCom(port)
End Sub

'**************************************************************************
'
' Usi2 functions for VB
'
'**************************************************************************
'---------------------------------------------------------------
'Usi2 Send data, receive data, exchange data
'---------------------------------------------------------------
Public Function vbSendData(port As Long, strCmd As String, strDataBuf As String) As Long
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   
   'If strCmd = "" Then
   '   vbusiSendData% = -6
   '   Exit Function
   'End If
   
   bCmd = Val("&H" & strCmd)
   
   DataLen = HexStrToBin(strDataBuf, baSend)
   
   Call usiSendData(port, bCmd, DataLen, baSend(0))
   
   vbSendData = 0
End Function


Public Function vbReceiveData(port As Long, strRcvData As String) As Long
   Dim baReceive(512) As Byte
   Dim Rtn As Long
   
   Rtn = usiReceiveData(port, baReceive(0))
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbReceiveData = Rtn
End Function


Public Function vbExchange(port As Long, strCmd As String, _
                                strSndData As String, strRcvData As String) As Long
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim baReceive(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   
   'If strCmd = "" Then
   '   vbusiExchange% = -6
   '   Exit Function
   'End If
   
   bCmd = Val("&H" & strCmd)
   
   DataLen = HexStrToBin(strSndData, baSend)
   
   Rtn = usiExchange(port, bCmd, DataLen, baSend(0), baReceive(0))
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbExchange = Rtn
End Function

Public Function vbCheckCardPresence(port As Long) As Long
    vbCheckCardPresence = usiCheckCardPresence(port)
End Function


Public Function vbusiCardMoveNotify(port As Long, strOUT As String) As Long
Dim baReceive(512) As Byte
Dim Rtn As Long
   
   Rtn = usiCardMoveNotify(port, baReceive(0))
   
   If Rtn >= 0 Then
      strOUT = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
    vbusiCardMoveNotify = Rtn
End Function


'---------------------------------------------------------------
'Usi2
'---------------------------------------------------------------
''''Public Function vbNotifyData(Port As Long, strRcvData As String) As Long
''''   Dim baReceive(512) As Byte
''''   Dim Rtn As Long
''''
''''   Rtn = usiNotifyData(Port, baReceive(0))
''''
''''   If Rtn >= 0 Then
''''      strRcvData = BinToHexStr(baReceive, Rtn)
''''      Rtn = Rtn * 2   'String length unpack to 2 times
''''   End If
''''
''''   vbNotifyData = Rtn
''''End Function

''''Public Function vbusiGetErrCode() As Byte
''''   vbusiGetErrCode = usiGetErrCode
''''End Function

'---------------------------------------------------------------
'Reader command
'---------------------------------------------------------------
Public Function vbReaderVerReport(port As Long, ByVal strCmd As String, strRcvData As String) As Long
   'strCmd is Hex string -> "39"
   Dim bCmd As Byte
   Dim baReceive(512) As Byte
   Dim Rtn As Long
   
    If strCmd <> "" Then
        bCmd = Val("&H" & strCmd)
    Else
        bCmd = 0
    End If
   
   Rtn = usiReaderVerReport(port, bCmd, baReceive(0))
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbReaderVerReport = Rtn

End Function

Public Function vbRetransmit(port As Long, strRcvData As String) As Long
Dim baReceive(512) As Byte
Dim Rtn As Long

    Rtn = usiRetransmit(port, baReceive(0))
    
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbRetransmit = Rtn
End Function

Public Function vbusiSwitchStatus(port As Long) As Long
    
    vbusiSwitchStatus = usiSwitchStatus(port)
End Function

Public Function vbusiSetVerboseMode(port As Long) As Long
    
    vbusiSetVerboseMode = usiSetVerboseMode(port)
End Function

Public Function vbusiReaderStatus(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(4) As Byte
Dim Rtn As Long
    
    Rtn = usiReaderStatus(port, baReceive(0))
    
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
    
    vbusiReaderStatus = Rtn
End Function

Public Function vbGreenLedON(port As Long) As Long
    
    vbGreenLedON = usiSetGreenLed(port, 1) '1=ON
End Function

Public Function vbGreenLedOFF(port As Long) As Long
    
    vbGreenLedOFF = usiSetGreenLed(port, 0) '0=off
End Function

Public Function vbGreenLedFLASH(port As Long) As Long
    
    vbGreenLedFLASH = usiSetGreenLed(port, 2) '2=flash
End Function

Public Function vbRedLedON(port As Long) As Long
    
    vbRedLedON = usiSetRedLed(port, 1)
End Function

Public Function vbRedLedOFF(port As Long) As Long
    
    vbRedLedOFF = usiSetRedLed(port, 0)
End Function

Public Function vbRedLedFLASH(port As Long) As Long
        
    vbRedLedFLASH = usiSetRedLed(port, 2)
End Function

Public Function vbusiLatch(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(50) As Byte
Dim Rtn As Long
    
    Rtn = usiLatch(port, baReceive(0))
    
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
    
    vbusiLatch = Rtn
End Function

Public Function vbusiUnlatch(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(50) As Byte
Dim Rtn As Long
    
    Rtn = usiUnlatch(port, baReceive(0))
    
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
    
    vbusiUnlatch = Rtn

End Function

Public Function vbusiConfigReport(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(32) As Byte
Dim Rtn As Long
    
    Rtn = usiConfigReport(port, baReceive(0))
    
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
    
    vbusiConfigReport = Rtn

End Function

Public Function vbusiWarmReset(ByVal port As Long) As Long
    vbusiWarmReset = usiWarmReset(port)
End Function

'Public Function vbGetReaderMaskCode(Port As Long, strMaskCode As String) As long
'   Dim Rtn As long
'   Dim Mask As String * 256 'Allocate 256 bytes memory
'
'   Rtn = GetReaderMaskCode(Port, Mask)
'
'   If Rtn >= 0 Then
'      strMaskCode = Mask
'   End If
'
'   vbGetReaderMaskCode% = Rtn
'End Function

'---------------------------------------------------------------
'Usi2 Magnetic card
'---------------------------------------------------------------
Public Function vbusiArmtoRead(ByVal port As Long) As Long
    vbusiArmtoRead = usiArmtoRead(port)
End Function

Public Function vbusiAbort(ByVal port As Long) As Long
    vbusiAbort = usiAbort(port)
End Function
Public Function vbusiReadMagData(ByVal port As Long, ByVal selectTK As Byte, strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long
    Rtn = usiReadMagData(port, selectTK, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiReadMagData = Rtn
End Function
Public Function vbusiTK1Data(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long

    Rtn = usiTK1Data(port, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiTK1Data = Rtn
End Function
Public Function vbusiTK2Data(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long
    Rtn = usiTK2Data(port, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiTK2Data = Rtn
End Function
Public Function vbusiTK3Data(ByVal port As Long, strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long
    Rtn = usiTK3Data(port, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiTK3Data = Rtn
End Function
Public Function vbusiISOData(ByVal port As Long, ByVal Track As Byte, ByVal viaISO As Byte, _
                                strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long
    
'    If strCmd <> "" Then
'        bCmd = Val("&H" & strCmd)
'    Else
'        bCmd = 0
'    End If
'
    Rtn = usiISOData(port, Track, viaISO, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiISOData = Rtn

End Function
Public Function vbusiForwardCusData(ByVal port As Long, ByVal Track As Byte, ByVal viaNull As Byte, _
                                    ByVal chrNums As Byte, strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long
    Rtn = usiForwardCusData(port, Track, viaNull, chrNums, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiForwardCusData = Rtn
End Function
Public Function vbusiReverseCusData(ByVal port As Long, ByVal Track As Byte, ByVal viaNull As Byte, _
                                    ByVal chrNums As Byte, strRcvData As String) As Long
Dim baReceive(256) As Byte
Dim Rtn As Long
    Rtn = usiReverseCusData(port, Track, viaNull, chrNums, baReceive(0))
        
    If Rtn >= 0 Then
        strRcvData = BinToHexStr(baReceive, Rtn)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If

    vbusiReverseCusData = Rtn
End Function


'---------------------------------------------------------------
'Usi2 CPU card
'---------------------------------------------------------------
'IccConnector = 0~4
Public Function vbusiSelectIccConnector(port As Long, IccConnector As Byte) As Long
    vbusiSelectIccConnector = usiSelectIccConnector(port, IccConnector)
End Function

Public Function vbCardPowerUp(port As Long, Vcc As Byte, EmvComply As Byte, strAtr As String) As Long
   Dim baAtr(64) As Byte
   Dim Rtn As Long
'BYTE Vcc[] : (1/3/5) V1_8/V3/V5, two byte
'Emv_PPSComply : x0: ISO Mode, x1: EMV Mode
'                0x: PPS T=0, 1X: PPS T=1
   
   Rtn = usiCardPowerUp(port, Vcc, EmvComply, baAtr(0)) 'Vcc = V5 or V3
   
   If Rtn >= 0 Then
      strAtr = BinToHexStr(baAtr, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbCardPowerUp = Rtn
End Function

Public Function vbCardPowerOff(port As Long) As Long
    vbCardPowerOff = usiCardPowerOff(port)
End Function

Public Function vbSendApdu(port As Long, strApdu As String, strOUT As String, strSW As String) As Long
   Dim baApdu(264) As Byte
   Dim baReceive(512) As Byte
   Dim baSW(8) As Byte
   Dim nAPDULen As Integer
   Dim Rtn As Long
   
   'If strApdu = "" Then
   '   vbSendApdu% = -6
   '   Exit Function
   'End If
   
   nAPDULen = HexStrToBin(strApdu, baApdu)
   
   Rtn = usiSendApdu(port, nAPDULen, baApdu(0), baReceive(0), baSW(0))
   
   If Rtn >= 0 Then
      strOUT = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbSendApdu = Rtn
End Function

'---------------------------------------------------------------
'Usi2 Memory card
'---------------------------------------------------------------
Public Function vbusiSelectMemCardType(ByVal port As Long, ByVal CardType As Byte, ByVal PageSize As Byte) As Long

    vbusiSelectMemCardType = usiSelectMemCardType(port, CardType, PageSize)

End Function

Public Function vbusiSendMemCardApdu(port As Long, strApdu As String, strOUT As String, strSW As String) As Long
   Dim baApdu(264) As Byte
   Dim baReceive(512) As Byte
   Dim baSW(8) As Byte
   Dim nAPDULen As Integer
   Dim Rtn As Long
   
   'If strApdu = "" Then
   '   vbSendApdu% = -6
   '   Exit Function
   'End If
   
   nAPDULen = HexStrToBin(strApdu, baApdu)
   
   Rtn = usiSendMemCardApdu(port, nAPDULen, baApdu(0), baReceive(0), baSW(0))
   
   If Rtn >= 0 Then
      strOUT = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiSendMemCardApdu = Rtn

End Function

Public Function vbusiReportMemCardType(ByVal port As Long) As Long

End Function

Public Function vbusiMemoryVerify(ByVal port As Long, strPassWord As String, strSW As String) As Long
Dim baPassWord(16) As Byte
Dim baSW(8) As Byte
Dim nPSWLen As Integer
Dim Rtn As Long
   
   nPSWLen = HexStrToBin(strPassWord, baPassWord)
   
   Rtn = usiMemoryVerify(port, nPSWLen, baPassWord(0), baSW(0))
   
   If Rtn >= 0 Then
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryVerify = Rtn

End Function

Public Function vbusiMemoryAuthenticate(ByVal port As Long, strDataBuf As String, strSW As Byte) As Long
Dim baDataBuf(264) As Byte
Dim baSW(8) As Byte
Dim nDataLen As Integer
Dim Rtn As Long
   
   nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryAuthenticate(port, nDataLen, baDataBuf(0), baSW(0))
   
   If Rtn >= 0 Then
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryAuthenticate = Rtn

End Function

Public Function vbusiMemoryExtAuthenticate(ByVal port As Long, strDataBuf As String, strSW As String) As Long
Dim baDataBuf(264) As Byte
Dim baSW(8) As Byte
Dim nDataLen As Integer
Dim Rtn As Long
   
   nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryExtAuthenticate(port, nDataLen, baDataBuf(0), baSW(0))
   
   If Rtn >= 0 Then
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryExtAuthenticate = Rtn

End Function

Public Function vbusiMemoryReadData(ByVal port As Long, Addr As Integer, rcvLen As Integer, _
                                    strOUT As String, strSW As String) As Long
Dim baReceive(512) As Byte
Dim baSW(8) As Byte
Dim Rtn As Long
   
   'nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryReadData(port, Addr, rcvLen, baReceive(0), baSW(0))
   
   If Rtn >= 0 Then
      strOUT = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryReadData = Rtn
End Function

Public Function vbusiMemoryWriteData(ByVal port As Long, Addr As Integer, _
                                        strDataBuf As String, strSW As String) As Long
Dim baDataBuf(512) As Byte
Dim baSW(8) As Byte
Dim nDataLen As Integer
Dim Rtn As Long
   
   nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryWriteData(port, Addr, nDataLen, baDataBuf(0), baSW(0))
   
   If Rtn >= 0 Then
'      strOut = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryWriteData = Rtn
End Function

Public Function vbusiMemoryEraseData(ByVal port As Long, Addr As Integer, strSW As String) As Long
'Dim baDataBuf(512) As Byte
Dim baSW(8) As Byte
'Dim nDataLen As Integer
Dim Rtn As Long
   
   'nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryEraseData(port, Addr, baSW(0))
   
   If Rtn >= 0 Then
'      strOut = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryEraseData = Rtn
End Function

Public Function vbusiMemoryRestoreData(ByVal port As Long, Addr As Integer, strSW As String) As Long
'Dim baDataBuf(512) As Byte
Dim baSW(8) As Byte
'Dim nDataLen As Integer
Dim Rtn As Long
   
   'nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryRestoreData(port, Addr, baSW(0))
   
   If Rtn >= 0 Then
'      strOut = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryRestoreData = Rtn

End Function

Public Function vbusiMemoryWriteProtect(ByVal port As Long, Addr As Integer, strDataBuf As String, _
                                                 strSW As String) As Long
Dim baDataBuf(512) As Byte
Dim baSW(8) As Byte
Dim nDataLen As Integer
Dim Rtn As Long
   
   nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryWriteProtect(port, Addr, nDataLen, baDataBuf(0), baSW(0))
   
   If Rtn >= 0 Then
      'strOut = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryWriteProtect = Rtn

End Function

Public Function vbusiMemoryReadProtect(ByVal port As Long, Addr As Integer, rcvLen As Integer, _
                                        strOUT As String, strSW As String) As Long
Dim baReceive(512) As Byte
Dim baSW(8) As Byte
Dim Rtn As Long
   
   'nDataLen = HexStrToBin(strDataBuf, baDataBuf)
   
   Rtn = usiMemoryReadProtect(port, Addr, rcvLen, baReceive(0), baSW(0))
   
   If Rtn >= 0 Then
      strOUT = BinToHexStr(baReceive, Rtn)
      strSW = BinToHexStr(baSW, 2)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbusiMemoryReadProtect = Rtn
End Function

Public Function vbusiMemoryEraseUserArea(ByVal port As Long, ByVal Nums As Integer, _
                                            strErsPSC As String, strSW As String) As Long
Dim baDataBuf(16) As Byte
Dim baSW(8) As Byte
Dim Rtn As Long
Dim nDataLen As Integer
   
    nDataLen = HexStrToBin(strErsPSC, baDataBuf)
    
    Rtn = usiMemoryEraseUserArea(port, nDataLen, baDataBuf(0), baSW(0))
   
    If Rtn >= 0 Then
        'strOut = BinToHexStr(baReceive, Rtn)
        strSW = BinToHexStr(baSW, 2)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
   
    vbusiMemoryEraseUserArea = Rtn
    
End Function

Public Function vbusiMemoryChangePSC(ByVal port As Long, strNewPSC As String, strSW As String) As Long
Dim baDataBuf(12) As Byte
Dim baSW(8) As Byte
Dim Rtn As Long
Dim nDataLen As Integer
   
    nDataLen = HexStrToBin(strNewPSC, baDataBuf)
    
    Rtn = usiMemoryChangePSC(port, nDataLen, baDataBuf(0), baSW(0))
   
    If Rtn >= 0 Then
        'strOut = BinToHexStr(baReceive, Rtn)
        strSW = BinToHexStr(baSW, 2)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
   
    vbusiMemoryChangePSC = Rtn
    
End Function

Public Function vbusiMemoryReadErrCount(ByVal port As Long, strOUT As String, strSW As String) As Long
Dim baReceive(20) As Byte
Dim baSW(8) As Byte
Dim Rtn As Long
Dim nDataLen As Integer
   
    Rtn = usiMemoryReadErrCount(port, baReceive(0), baSW(0))
   
    If Rtn >= 0 Then
        strOUT = BinToHexStr(baReceive, Rtn)
        strSW = BinToHexStr(baSW, 2)
        Rtn = Rtn * 2   'String length unpack to 2 times
    End If
   
    vbusiMemoryReadErrCount = Rtn
End Function



'Public Function vbReadEeprom(Port As Long, Addr As Byte, Nums As Byte, strEeprom As String) As Long
'   Dim Rtn As Long
'   Dim baEeprom(128) As Byte
'
' '  Rtn = ReadEeprom(Port, Addr, Nums, baEeprom(0))
'
'   If Rtn >= 0 Then
'      strEeprom = BinToHexStr(baEeprom, Rtn)
'      Rtn = Rtn * 2   'String length unpack to 2 times
'   End If
'
'   vbReadEeprom = Rtn
'End Function
'
'Public Function vbReadMagStripe(Port As Long, Track As Byte, strData As String) As Long
'   Dim Rtn As Long
'   Dim baData(256) As Byte
'
''   Rtn = ReadMagStripe(Port, Track, baData(0))
'
'   If Rtn >= 0 Then
'      strData = BinToHexStr(baData, Rtn)
'      Rtn = Rtn * 2   'String length unpack to 2 times
'   End If
'
'   vbReadMagStripe = Rtn
'End Function

'---------------------------------------------------------------
'TLP224Turbo Protocol: Send data, receive data, exchange data
'---------------------------------------------------------------
Public Sub vbTLP224Turbo_SendData(port As Long, strCmd As String, strDataBuf As String)
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   Dim strSendData As String
   
   strSendData = strCmd + strDataBuf
   
   DataLen = HexStrToBin(strSendData, baSend)
   
   Call TLP224Turbo_SendData(port, baSend(0), DataLen, baSend(1))
   
End Sub


Public Function vbTLP224Turbo_ReceiveData(port As Long, strRcvData As String, _
                                          lDelayTime As Long) As Long
   Dim baReceive(512) As Byte
   Dim Rtn As Long
   
   Rtn = TLP224Turbo_ReceiveData(port, baReceive(0), lDelayTime)
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbTLP224Turbo_ReceiveData = Rtn
End Function


Public Function vbTLP224Turbo_Exchange(port As Long, strCmd As String, _
                                strSndData As String, strRcvData As String, _
lDelayTime As Long) As Long
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim baReceive(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   Dim strSendData As String
   
   strSendData = strCmd + strDataBuf
   
   DataLen = HexStrToBin(strSendData, baSend)
   
   Rtn = TLP224Turbo_Exchange(port, baSend(0), DataLen, baSend(1), baReceive(0), _
lDelayTime)
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   VB TLP224Turbo_Exchange = Rtn
End Function



'---------------------------------------------------------------
'TLP224 Protocol: Send data, receive data, exchange data
'---------------------------------------------------------------
Public Sub vbTLP224T_SendData(port As Long, strCmd As String, strDataBuf As String)
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   Dim strSendData As String
   
   strSendData = strCmd + strDataBuf
   
   DataLen = HexStrToBin(strSendData, baSend)
   
   Call TLP224_SendData(port, baSend(0), DataLen, baSend(1))
   
End Sub


Public Function vbTLP224_ReceiveData(port As Long, strRcvData As String, _
                                     lDelayTime As Long) As Long
   Dim baReceive(512) As Byte
   Dim Rtn As Long
   
   Rtn = TLP224_ReceiveData(port, baReceive(0), lDelayTime)
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbTLP224_ReceiveData = Rtn
End Function


Public Function vbTLP224_Exchange(port As Long, strCmd As String, _
                                strSndData As String, strRcvData As String, _
lDelayTime As Long) As Long
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim baReceive(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   Dim strSendData As String
   
   strSendData = strCmd + strDataBuf
   
   DataLen = HexStrToBin(strSendData, baSend)
   
   Rtn = TLP224_Exchange(port, baSend(0), DataLen, baSend(1), baReceive(0), _
lDelayTime)
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbTLP224_Exchange = Rtn
End Function




'---------------------------------------------------------------
'USI1 Protocol: Send data, receive data, exchange data
'---------------------------------------------------------------
Public Sub vbUSI1_SendData(port As Long, strDataBuf As String)
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   
 
   DataLen = HexStrToBin(strDataBuf, baSend)
   
   Call USI1_SendData(port, DataLen, baSend(0))
   
End Sub


Public Function vbUSI1_ReceiveData(port As Long, strRcvData As String, _
                                   lDelayTime As Long) As Long
   Dim baReceive(512) As Byte
   Dim Rtn As Long
   
   Rtn = USI1_ReceiveData(port, baReceive(0), lDelayTime)
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbUSI1_ReceiveData = Rtn
End Function


Public Function vbUSI1_Exchange(port As Long, _
                                strSndData As String, strRcvData As String, _
lDelayTime As Long) As Long
   Dim bCmd As Byte
   Dim baSend(512) As Byte
   Dim baReceive(512) As Byte
   Dim DataLen As Integer
   Dim Rtn As Long
   
   
   DataLen = HexStrToBin(strSndData, baSend)
   
   Rtn = USI1_Exchange(port, DataLen, baSend(0), baReceive(0), _
lDelayTime)
   
   If Rtn >= 0 Then
      strRcvData = BinToHexStr(baReceive, Rtn)
      Rtn = Rtn * 2   'String length unpack to 2 times
   End If
   
   vbUSI1_Exchange = Rtn
End Function





