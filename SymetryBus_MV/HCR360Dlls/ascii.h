#ifndef __ASCII_H__
#define __ASCII_H__

#define SOH     0x01
#define STX     0x02
#define ETX     0x03
#define EOT     0x04
#define ENQ     0x05
#define ACK     0x06
#define BS      0x08
#define LF      0x0a
#define FF      0x0c
#define CR      0x0d
#define DLE     0x10
#define DC2     0x12
#define NAK     0x15
#define SYN     0x16
#define ETB     0x17
#define CAN     0x18
#define CTLZ    0x1a
#define ESC     0x1b
#define FS      0x1c
#define US      0x1F
#define SPACE   0x20

#endif
