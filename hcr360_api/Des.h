/*
**  Des.h
**
**  Use for both Win16 & Win32.
*/

#ifdef WIN32
  #ifdef DLL_SOURCE_CODE
     #define DLL_IMPORT_EXPORT __declspec(dllexport) __stdcall
  #else
     #define DLL_IMPORT_EXPORT __declspec(dllimport) __stdcall
  #endif
#else
  #define DLL_IMPORT_EXPORT FAR PASCAL
#endif

#ifdef __cplusplus
  #define NoMangle extern "C"
#else
  #define NoMangle
#endif

NoMangle int DLL_IMPORT_EXPORT Pack(LPSTR,int);
NoMangle int DLL_IMPORT_EXPORT Pack_Dat(LPSTR,unsigned char *,int);
NoMangle int DLL_IMPORT_EXPORT Unpack(LPSTR,int);
NoMangle int DLL_IMPORT_EXPORT Des(int,LPSTR,LPSTR,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Xorf(LPSTR,LPSTR,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Pmac(LPSTR,LPSTR,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Lrcf(LPSTR,int);
NoMangle int DLL_IMPORT_EXPORT FreeSR(int,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT FreeSRD(int,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Free800(int,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Free900(int,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Pmac1(LPSTR,LPSTR,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Pmac2(LPSTR,LPSTR,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Pmac3(LPSTR,LPSTR,LPSTR,int,LPSTR);
NoMangle int DLL_IMPORT_EXPORT Fmac(LPSTR,LPSTR,LPSTR,int,LPSTR);



