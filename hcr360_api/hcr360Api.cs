﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace hcr360_api
{
    public class hcr360Api
    {
        #region SDK

        #region CONSTANTES

        // COM port codes
        public const short HCR360_COM1 = 0;
        public const short HCR360_COM2 = 1;
        public const short HCR360_COM3 = 2;
        public const short HCR360_COM4 = 3;
        public const short HCR360_COM5 = 4;
        public const short HCR360_COM6 = 5;
        public const short HCR360_COM7 = 6;
        public const short HCR360_COM8 = 7;
        public const short HCR360_COM9 = 8;
        public const short HCR360_COM10 = 9;
        public const short HCR360_COM11 = 10;
        public const short HCR360_COM12 = 11;
        public const short HCR360_COM13 = 12;
        public const short HCR360_COM14 = 13;
        public const short HCR360_COM15 = 14;
        public const short HCR360_COM16 = 15;


        // Parity Codes
        public const short HCR360_NoParity = 0;
        public const short HCR360_OddParity = 1;
        public const short HCR360_EvenParity = 2;
        public const short HCR360_MarkParity = 3;
        public const short HCR360_SpaceParity = 4;


        // Stop Bit Codes
        public const short HCR360_OneStopBit = 0;
        public const short HCR360_TwoStopBits = 2;

        // Word Length Codes
        public const short HCR360_WordLength5 = 5;
        public const short HCR360_WordLength6 = 6;
        public const short HCR360_WordLength7 = 7;
        public const short HCR360_WordLength8 = 8;

        // baud codes
        public const short HCR360_BAUD1200 = 2;
        public const short HCR360_BAUD2400 = 3;
        public const short HCR360_BAUD4800 = 4;
        public const short HCR360_BAUD9600 = 5;
        public const short HCR360_BAUD19200 = 6;
        public const short HCR360_BAUD38400 = 7;  //'(*)
        public const short HCR360_BAUD57600 = 8;
        public const short HCR360_BAUD115200 = 9;

        // Rem Define IC Card VCC
        public const byte V1_8 = 1;
        public const byte V3 = 3;
        public const byte V5 = 5;

        #endregion

        #region Funciones

        //[DllImport("HCR360.dll")]
        [DllImportAttribute("HCR360.dll", EntryPoint = "OpenCom", CallingConvention = CallingConvention.StdCall)]
        public static extern int OpenCom(int port, int baud, int Databits);

        [DllImportAttribute("HCR360.dll", EntryPoint = "CloseCom", CallingConvention = CallingConvention.StdCall)]
        public static extern void CloseCom(int port);

        // Usi2
        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SendData", CallingConvention = CallingConvention.StdCall)]
        public static extern void usi2SendData(int port, Byte cmd, int DataLen, Byte DataBuf);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReceiveData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReceiveData(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2Exchange", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2Exchange(int port, Byte cmd, int SndDataLen, Byte SndData, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2CheckCardPresence", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2CheckCardPresence(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2CardMoveNotify", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2CardMoveNotify(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2Retransmit", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2Retransmit(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReaderVerReport", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReaderVerReport(int port, Byte SubCmd, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SwitchStatus", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SwitchStatus(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SetVerboseMode", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SetVerboseMode(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReaderStatus", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReaderStatus(int port, [MarshalAs(UnmanagedType.LPArray)] byte[] RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SetGreenLed", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SetGreenLed(int port, Byte LedStatus);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SetRedLed", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SetRedLed(int port, Byte LedStatus);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2Latch", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2Latch(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2Unlatch", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2Unlatch(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ConfigReport", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ConfigReport(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2WarmReset", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2WarmReset(int port);


        // Reader and CPU card
        // IC card
        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2CardPowerUp", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2CardPowerUp(int port, Byte Vcc, Byte EmvComply, Byte Atr);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2CardPowerOff", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2CardPowerOff(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SendApdu", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SendApdu(int port, int ApduLen, Byte ApduCmd, Byte RcvData, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SelectIccConnector", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SelectIccConnector(int port, Byte IccConnector);

        // Memory Card
        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SelectMemCardType", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SelectMemCardType(int port, Byte MemCardType, Byte PageSize);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2SendMemCardApu", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2SendMemCardApu(int port, int ApduLen, Byte ApduCmd, Byte RcvData, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReportMemCardType", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReportMemCardType(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryVerify", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryVerify(int port, int Nums, Byte Password, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryAuthenticate", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryAuthenticate(int port, int Nums, Byte DataBuf, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryExAuthenticate", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryExAuthenticate(int port, int Nums, Byte DataBuf, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryReadData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryReadData(int port, int Addr, int Nums, [MarshalAs(UnmanagedType.LPArray)] byte[] RcvData, [MarshalAs(UnmanagedType.LPArray)] byte[] SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryWriteData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryWriteData(int port, int Addr, int Nums, Byte DataBuf, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryEraseData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryEraseData(int port, int Addr, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2RestoreData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2RestoreData(int port, int Addr, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryWriteProtect", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryWriteProtect(int port, int Addr, int Nums, Byte DataBuf, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemroyReadProtect", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemroyReadProtect(int port, int Addr, int Nums, Byte RcvData, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2MemoryEraseUserArea", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2MemoryEraseUserArea(int port, int Nums, Byte ErsPSC, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ChangePSC", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ChangePSC(int port, int Nums, Byte NewPsc, Byte SW);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReadErrCount", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReadErrCount(int port, Byte ErrCounte, Byte SW);

        // Mag card cm
        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ArmtoRead", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ArmtoRead(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2Abort", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2Abort(int port);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReadMagData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReadMagData(int port, Byte selectTK, [MarshalAs(UnmanagedType.LPArray)] byte[] RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2TK1Data", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2TK1Data(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2TK2Data", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2TK2Data(int port, [MarshalAs(UnmanagedType.LPArray)] byte[] RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2TK3Data", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2TK3Data(int port, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ISOData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ISOData(int port, Byte Track, Byte viaISO, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2FowardCusData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2FowardCusData(int port, Byte Track, Byte viaNull, Byte chrNums, Byte RcvData);

        [DllImportAttribute("HCR360.dll", EntryPoint = "usi2ReverseCusData", CallingConvention = CallingConvention.StdCall)]
        public static extern int usi2ReverseCusData(int port, Byte Track, Byte viaNull, Byte chrNums, Byte RcvData);

        // TLP224 Turbo Protocol
        [DllImportAttribute("HCR360.dll", EntryPoint = "TLP224Turbo_SendData", CallingConvention = CallingConvention.StdCall)]
        public static extern void TLP224Turbo_SendData(int port, Byte Cmd, int DataLen, Byte DataBuf);

        [DllImportAttribute("HCR360.dll", EntryPoint = "TLP224Turbo_ReceiveData", CallingConvention = CallingConvention.StdCall)]
        public static extern int TLP224Turbo_ReceiveData(int port, Byte RcvData, long lDelayTime);

        [DllImportAttribute("HCR360.dll", EntryPoint = "TLP224Turbo_Exchange", CallingConvention = CallingConvention.StdCall)]
        public static extern int TLP224Turbo_Exchange(int port, Byte Cmd, int SndDataLen, Byte RdvData, long lDelayTime);

        // TLP334 Protocol
        [DllImportAttribute("HCR360.dll", EntryPoint = "TLP224_SendData", CallingConvention = CallingConvention.StdCall)]
        public static extern void TLP224_SendData(int port, Byte Cmd, int DataLen, Byte DataBuf);

        [DllImportAttribute("HCR360.dll", EntryPoint = "TLP224_ReceiveData", CallingConvention = CallingConvention.StdCall)]
        public static extern int TLP224_ReceiveData(int port, Byte RcvData, long lDelayTime);

        [DllImportAttribute("HCR360.dll", EntryPoint = "TLP224_Exchange", CallingConvention = CallingConvention.StdCall)]
        public static extern int TLP224_Exchange(int port, Byte Cmd, int SndDataLen, Byte SndData, Byte RcvData, long lDelayTime);

        // USI1 Protocol
        [DllImportAttribute("HCR360.dll", EntryPoint = "USI1_SendData", CallingConvention = CallingConvention.StdCall)]
        public static extern void USI1_SendData(int port, Byte Cmd, int DataLen, Byte DataBuf);

        [DllImportAttribute("HCR360.dll", EntryPoint = "USI1_ReceiveData", CallingConvention = CallingConvention.StdCall)]
        public static extern int USI1_ReceiveData(int port, Byte RcvData, long lDelayTime);

        [DllImportAttribute("HCR360.dll", EntryPoint = "USI1", CallingConvention = CallingConvention.StdCall)]
        public static extern int USI1(int port, Byte Cmd, int SndDataLen, Byte SndData, Byte RcvData, long lDelayTime);

        #endregion

        #endregion

        public static int ReadMagData(int port, Byte selectTK, byte[] RcvData)
        {
            int ReturnCode = usi2ReadMagData(port, selectTK, RcvData);
            return ReturnCode;
        }

        public static int AbrirPuerto(int Puerto)
        {
            int ReturnCode = OpenCom(Puerto, 7, 4);

            return ReturnCode;
        }

        public static void CerrarPuerto(int Puerto)
        {
            CloseCom(Puerto);
        }

        public static int LeerEstatus(int Puerto, byte[] DatosRecibidos)
        {
            int ReturnCode = usi2ReaderStatus(Puerto, DatosRecibidos);

            return ReturnCode;
        }

        public static bool TarejtaInsertada(int Puerto)
        {
            int ReturnCode = usi2CheckCardPresence(Puerto);

            if (ReturnCode == 1)
                return true;
            else
                return false;
        }

        public static int CambiarTipoTarjeta(int Puerto, byte TipoTarjeta)
        {
            int ReturnCode = usi2SelectMemCardType(Puerto, TipoTarjeta, 0);

            return ReturnCode;
        }

        public static int LeerDatos(int Puerto, byte[] DatosRecibidos, byte[] SW)
        {
            int ReturnCode = usi2MemoryReadData(Puerto, 40, 6, DatosRecibidos, SW);

            return ReturnCode;
        }

        public static int CambiarEstatus(int Puerto)
        {
            int ReturnCode = usi2SwitchStatus(Puerto);

            return ReturnCode;
        }
    }
}
