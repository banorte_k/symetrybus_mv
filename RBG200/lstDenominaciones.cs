﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBG200
{
    public class lstDenominaciones
    {
        double denominacion, cantidad, item;

        public lstDenominaciones(double denominacion, double cantidad, double item)
        {
            this.denominacion = denominacion;
            this.cantidad = cantidad;
            this.item = item;
        }

        public double Denominacion
        {
            get
            {
                return denominacion;
            }

            set
            {
                denominacion = value;
            }
        }

        public double Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Item
        {
            get
            {
                return item;
            }

            set
            {
                item = value;
            }
        }
    }
}
