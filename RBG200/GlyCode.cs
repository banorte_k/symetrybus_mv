﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBG200
{
    public class GlyCode
    {
        #region GLYCODE STRING
        public static readonly Dictionary<int, string> GLY_CODE_STRING = new Dictionary<int, string>()
        {
            {1048576,"GLY_RBU_OFFSET "},
            {1114112,"GLY_PD61_OFFSET "},
            {1179648,"GLY_PD600_OFFSET "},
            {1245184,"GLY_RZ100_OFFSET "},
            {1310720,"GLY_RZ50_OFFSET "},
            {1376256,"GLY_RBW10_OFFSET "},
            {1441792,"GLY_RCW8_OFFSET "},
            {1507328,"GLY_RBGX3_OFFSET "},
            {1572864,"GLY_RCN50_OFFSET "},
            {1638400,"GLY_UW_OFFSET "},
            {1703936,"GLY_RBG100_OFFSET "},
            {1769472,"GLY_DE_OFFSET "},
            {1789952,"GLY_UWWINCE_OFFSET "},
            {1835008,"GLY_MESSAGE_EX_OFFSET "},
            {1900544,"GLY_TELEQUIPTFLEX_OFFSET "},
            {1966080,"GLY_MPE700_OFFSET "},
            {2097152,"GLY_RB500_OFFSET "},
            {2117632,"GLY_SDR100_OFFSET "},
            {2162688,"GLY_RBG200_OFFSET "},
            {2228224,"GLY_RBWX_OFFSET "},
            {2293760,"GLY_RCWX_OFFSET "},
            {2359296,"GLY_USF_OFFSET "},
            {2424832,"GLY_JAVA_IF_OFFSET "},
            {2490368,"GLY_SOAP_SERVICE_OFFSET "},
            {2555904,"GLY_SDRB100_OFFSET "},
            {2621440,"GLY_SDRC100_OFFSET "},
            {2752512,"GLY_DSC_OFFSET "},
            {2760704,"GLY_DSB_OFFSET "},
            {2818048,"GLY_PBNX_OFFSET "},
            {3211264,"GLY_HVE200_OFFSET "},
            {3276800,"GLY_RBW150_OFFSET "},
            {3342336,"GLY_SCWX_OFFSET "},
            {0,"GLY_SUCCESS "},
            {1,"GLY_COMPLETE_MESSAGE "},
            {2,"GLY_CANCELED "},
            {3,"GLY_DEVICE_SPECIFIC_EVENT "},
            {4,"GLY_DEVICE_SPECIFIC_EVENT_BY_STRING "},
            {5,"GLY_HANDLE_CLOSED "},
            {6,"GLY_DEVICE_SPECIFIC_EVENT_EX "},
            {2001,"GLY_STATUS_CHANGE "},
            {2002,"GLY_EMPTY "},
            {2003,"GLY_EXIST "},
            {2004,"GLY_FULL "},
            {2005,"GLY_ERROR "},
            {2006,"GLY_STATUS_IDLE "},
            {2007,"GLY_STATUS_USING_OWN "},
            {2008,"GLY_STATUS_BUSY "},
            {2009,"GLY_STATUS_ERROR "},
            {2010,"GLY_STATUS_ERROR_COMMUNICATION "},
            {2011,"GLY_DEPOSIT_COUNT_CHANGE "},
            {2012,"GLY_REPLENISH_COUNT_CHANGE "},
            {2013,"GLY_NUMBER_COUNTING_COUNT_CHANGE "},
            {2015,"GLY_STATUS_DLL_INITIALIZE_BUSY "},
            {2017,"GLY_HIGH "},
            {2018,"GLY_STOP "},
            {2020,"GLY_MISSING "},
            {2021,"GLY_SHORTAGE "},
            {2025,"GLY_INTERMEDIATE_COLLECT_COUNT_CHANGE "},
            {2030,"GLY_CLOSED "},
            {2031,"GLY_OPENED "},
            {2016,"GLY_VERIFY_COUNT_CHANGE "},
            {2024,"GLY_REFILL_COUNT_CHANGE "},
            {2032,"GLY_COLLECT_COUNT_CHANGE "},
            {2014,"GLY_LOW "},
            {2023,"GLY_NA "},
            {3001,"GLY_WAIT_FOR_REMOVING "},
            {3002,"GLY_REMOVED "},
            {3003,"GLY_POWER_OFF_ON_REQUEST "},
            {3004,"GLY_PUSH_INUSE_KEY "},
            {3006,"GLY_ISSUE_RESET_COMMAND "},
            {4001,"GLY_ENTRANCE "},
            {4002,"GLY_EXIT "},
            {4003,"GLY_ESCROW "},
            {4004,"GLY_STACKER1 "},
            {4005,"GLY_STACKER2 "},
            {4006,"GLY_STACKER3 "},
            {4007,"GLY_STACKER4 "},
            {4008,"GLY_STACKER5 "},
            {4009,"GLY_STACKER6 "},
            {4010,"GLY_RJB "},
            {4025,"GLY_CASSETTE1 "},
            {4026,"GLY_CASSETTE2 "},
            {4027,"GLY_CASSETTE3 "},
            {4028,"GLY_CASSETTE4 "},
            {4029,"GLY_CASSETTE5 "},
            {4030,"GLY_CASSETTE6 "},
            {4141,"GLY_DOWNLOAD_PROGRESS "},
            {4142,"GLY_LOGREAD_PROGRESS "},
            {4155,"GLY_SAFE_DOOR "},
            {4166,"GLY_CASSETTE "},
            {4177,"GLY_STACKER7 "},
            {4178,"GLY_STACKER8 "},
            {4201,"GLY_EXIT_RJ "},
            {4202,"GLY_CASSETTE7 "},
            {4203,"GLY_CASSETTE8 "},
            {4211,"GLY_CATEGORY_1 "},
            {4212,"GLY_CATEGORY_2 "},
            {4213,"GLY_CATEGORY_3 "},
            {4214,"GLY_CATEGORY_4A "},
            {4215,"GLY_CATEGORY_4B "},
            {5010,"GET_IGNORE_STATE_FOR_OCX "},
            {7001,"GLY_CPS1_DISPENSE1 "},
            {7002,"GLY_CPS2_DISPENSE1 "},
            {7003,"GLY_CPS3_DISPENSE1 "},
            {7004,"GLY_CPS4_DISPENSE1 "},
            {7005,"GLY_CPS5_DISPENSE1 "},
            {7006,"GLY_CPS6_DISPENSE1 "},
            {7007,"GLY_CPS1_DISPENSE2 "},
            {7008,"GLY_CPS2_DISPENSE2 "},
            {7009,"GLY_CPS3_DISPENSE2 "},
            {7010,"GLY_CPS4_DISPENSE2 "},
            {7011,"GLY_CPS5_DISPENSE2 "},
            {7012,"GLY_CPS6_DISPENSE2 "},
            {7013,"GLY_CPS1_COUNTER "},
            {7014,"GLY_CPS2_COUNTER "},
            {7015,"GLY_CPS3_COUNTER "},
            {7016,"GLY_CPS4_COUNTER "},
            {7017,"GLY_CPS5_COUNTER "},
            {7018,"GLY_CPS6_COUNTER "},
            {7019,"GLY_CPS7_COUNTER "},
            {7020,"GLY_CPS8_COUNTER "},
            {7021,"GLY_CONTAINER_C4A_COUNTER "},
            {7022,"GLY_DEPOSIT_COUNT_MONITOR "},
            {7023,"GLY_CONTAINER_C1_COUNTER "},
            {7024,"GLY_CONTAINER_C2_COUNTER "},
            {7025,"GLY_CONTAINER_C3_COUNTER "},
            {7026,"GLY_CPS1_C3_COUNTER "},
            {7027,"GLY_CPS2_C3_COUNTER "},
            {7028,"GLY_CPS3_C3_COUNTER "},
            {7029,"GLY_CPS4_C3_COUNTER "},
            {7030,"GLY_CPS6_C3_COUNTER "},
            {7031,"GLY_CPS8_C3_COUNTER "},
            {7032,"GLY_CAPTUREBIN_COUNTER "},
            {7033,"GLY_CURRENCY_DEVICE "},
            {7034,"GLY_NUM_DEVICE_EXIT "},
            {7035,"GLY_MAX_DISPENSE_CNT "},
            {7036,"GLY_CONTAINER_C4B_COUNTER "},
            {7037,"GLY_STATUS_COUNTING "},
            {7038,"GLY_COLLECTION_BOX "},
            {7039,"GLY_COLLECT_BOX_INSERTED "},
            {7040,"GLY_REJECT_BOX "},
            {7053,"GLY_CAP_CANCEL_DEPOSIT "},
            {7054,"GLY_CAP_PAUSE_DEPOSIT "},
            {7055,"GLY_CAP_COMPARE_FIRM "},
            {7056,"GLY_CAP_UPDATE_FIRM "},
            {7057,"GLY_CAP_LOG_READ "},
            {7058,"GLY_CAP_LOG_CLEAR "},
            {7059,"GLY_CAP_DEPOSIT "},
            {7060,"GLY_CAP_DESCREPANCY "},
            {7061,"GLY_P_FIRMWARE_UNKNOWN "},
            {7062,"GLY_P_FIRMWARE_SAME "},
            {7063,"GLY_P_FIRMWARE_NEWER "},
            {7064,"GLY_P_FIRMWARE_OLDER "},
            {7065,"GLY_P_FIRMWARE_DIFFERENT "},
            {7066,"GLY_CHECK_COUNTER "},
            {7067,"DepositToCollection "},
            {7068,"GLY_RBG_NOTE_DATA_INFO "},
            {7069,"GLY_EXPOSED_FUND_COUNTER "},
            {-1,"GLY_INDEFINITE_WAIT, GLY_ERROR_SYSTEM_ERROR "},
            {-2,"GLY_ERROR_PORT_OPEN_FAIL "},
            {-3,"GLY_ERROR_PORT_CONFIG_ERROR "},
            {-4,"GLY_ERROR_DEVICE_NOT_FOUND "},
            {-5,"GLY_ERROR_PORT_NOT_FOUND "},
            {-6,"GLY_ERROR_TCP_HOST_NOT_FOUND "},
            {-7,"GLY_ERROR_INVALID_HANDLE "},
            {-8,"GLY_ERROR_INVALID_MESSAGE_CODE "},
            {-9,"GLY_ERROR_USER_WINDOW_MESSAGE_NOT_SET "},
            {-10,"GLY_ERROR_NOT_SUPPORTED_FUNCTION "},
            {-11,"GLY_ERROR_INVALID_ID "},
            {-12,"GLY_ERROR_NOT_SUPPORTED_CURRENCYID "},
            {-13,"GLY_ERROR_UDP_HOST_NOT_FOUND "},
            {-14,"GLY_ERROR_ID_NOT_FOUND "},
            {-15,"GLY_ERROR_NOT_OPEN "},
            {-16,"GLY_ERROR_ALREADY_OPEN "},
            {-18,"GLY_ERROR_DLL_NOT_FOUND "},
            {-19,"GLY_ERROR_DEVICE_CONFIG_ERROR "},
            {-20,"GLY_ERROR_FILE_NOT_FOUND "},
            {-21,"GLY_ERROR_ELEMENT_NOT_FOUND "},
            {-22,"GLY_ERROR_FILE_FORMAT "},
            {-23,"GLY_ERROR_DL_FILE_SUM_CHECK_ERROR "},
            {-24,"GLY_ERROR_DL_NEED_AGAIN "},
            {-25,"GLY_ERROR_LOGICAL_NAME_NOT_DEFINED "},
            {-26,"GLY_ERROR_CONFIG_FILE_NOT_FOUND "},
            {-27,"GLY_ERROR_INVALID_DLL "},
            {-101,"GLY_ERROR_SEQUENCE "},
            {-102,"GLY_ERROR_COMMUNICATION "},
            {-103,"GLY_ERROR_BUSY "},
            {-104,"GLY_ERROR_MECHA "},
            {-105,"GLY_ERROR_OPERATION "},
            {-106,"GLY_ERROR_PARAMETER "},
            {-107,"GLY_ERROR_INVALID_PARAMETER "},
            {-108,"GLY_ERROR_MONEY_NOT_MATCHED "},
            {-109,"GLY_ERROR_NOT_LOCKED "},
            {-110,"GLY_ERROR_TIMEOUT "},
            {-111,"GLY_ERROR_COM_PORT_IS_OCCUPIED "},
            {-112,"GLY_ERROR_INCOMPLETE "},
            {-113,"GLY_ERROR_SHORTAGE "},
            {-114,"GLY_ERROR_SYSTEM_LOCKED "},
            {-115,"GLY_ERROR_NOT_EJECT_BAIT "},
            {-116,"GLY_ERROR_REMOVE_END_STACKER "},
            {-1001,"GLY_ERROR_COMMUNICATION_TYPED_401 "},
            {-1002,"GLY_ERROR_COMMUNICATION_TYPED_402 "},
            {-1003,"GLY_ERROR_COMMUNICATION_TYPED_403 "},
            {-1004,"GLY_ERROR_COMMUNICATION_TYPED_404 "},
            {-1005,"GLY_ERROR_COMMUNICATION_TYPED_405 "},
            {-1006,"GLY_ERROR_COMMUNICATION_TYPED_406 "},
            {-1007,"GLY_ERROR_COMMUNICATION_TYPED_407 "},
            {-1008,"GLY_ERROR_COMMUNICATION_TYPED_408 "},
            {-1009,"GLY_ERROR_COMMUNICATION_TYPED_409 "},
            {-1010,"GLY_ERROR_COMMUNICATION_TYPED_410 "},
            {-1011,"GLY_ERROR_COMMUNICATION_TYPED_411 "},
            {-1012,"GLY_ERROR_COMMUNICATION_TYPED_412 "},
            {-1013,"GLY_ERROR_COMMUNICATION_TYPED_413 "},
            {-1014,"GLY_ERROR_COMMUNICATION_TYPED_414 "},
            {1048598,"GLY_ERROR_BILL_COUNTS "},
            {7041,"BillsToCaptureBin "},
            {7042,"RZ_GetSerialNumber "},
            {7043,"RZ_GetCstSerialNumber "},
            {7044,"RZ_SetSerialNumber "},
            {7045,"RZ_SetCstSerialNumber "},
            {7046,"DirectDispense "},
            {7047,"RejectDataRead "},
            {7048,"NoteDataRead "},
            {7049,"ModifyMessage "},
            {7050,"WriteInformation "},
            {7051,"ReadInformation "},
            {7052,"GetCountNumber "},
            {1245197,"RZ_Collect34B "},
            {1245190,"RemainsCollect "},
            {1507345,"GLY_BALANCE_COUNTER "},
            {1507329,"GLY_COUNT_START "},
            {1048597,"GLY_COUNTER_CORRECTION_COUNT_CHANGE "}
        };
        #endregion

        #region GLYCODE INT

        /// <summary>GLY_RBU_OFFSET = 1048576;</summary>
        public static int GLY_RBU_OFFSET = 1048576;
        /// <summary>GLY_PD61_OFFSET = 1114112;</summary>
        public static int GLY_PD61_OFFSET = 1114112;
        /// <summary>GLY_PD600_OFFSET = 1179648;</summary>
        public static int GLY_PD600_OFFSET = 1179648;
        /// <summary>GLY_RZ100_OFFSET = 1245184;</summary>
        public static int GLY_RZ100_OFFSET = 1245184;
        /// <summary>GLY_RZ50_OFFSET = 1310720;</summary>
        public static int GLY_RZ50_OFFSET = 1310720;
        /// <summary>GLY_RBW10_OFFSET = 1376256;</summary>
        public static int GLY_RBW10_OFFSET = 1376256;
        /// <summary>GLY_RCW8_OFFSET = 1441792;</summary>
        public static int GLY_RCW8_OFFSET = 1441792;
        /// <summary>GLY_RBGX3_OFFSET = 1507328;</summary>
        public static int GLY_RBGX3_OFFSET = 1507328;
        /// <summary>GLY_RCN50_OFFSET = 1572864;</summary>
        public static int GLY_RCN50_OFFSET = 1572864;
        /// <summary>GLY_UW_OFFSET = 1638400;</summary>
        public static int GLY_UW_OFFSET = 1638400;
        /// <summary>GLY_RBG100_OFFSET = 1703936;</summary>
        public static int GLY_RBG100_OFFSET = 1703936;
        /// <summary>GLY_DE_OFFSET = 1769472;</summary>
        public static int GLY_DE_OFFSET = 1769472;
        /// <summary>GLY_UWWINCE_OFFSET = 1789952;</summary>
        public static int GLY_UWWINCE_OFFSET = 1789952;
        /// <summary>GLY_MESSAGE_EX_OFFSET = 1835008;</summary>
        public static int GLY_MESSAGE_EX_OFFSET = 1835008;
        /// <summary>GLY_TELEQUIPTFLEX_OFFSET = 1900544;</summary>
        public static int GLY_TELEQUIPTFLEX_OFFSET = 1900544;
        /// <summary>GLY_MPE700_OFFSET = 1966080;</summary>
        public static int GLY_MPE700_OFFSET = 1966080;
        /// <summary>GLY_RB500_OFFSET = 2097152;</summary>
        public static int GLY_RB500_OFFSET = 2097152;
        /// <summary>GLY_SDR100_OFFSET = 2117632;</summary>
        public static int GLY_SDR100_OFFSET = 2117632;
        /// <summary>GLY_RBG200_OFFSET = 2162688;</summary>
        public static int GLY_RBG200_OFFSET = 2162688;
        /// <summary>GLY_RBWX_OFFSET = 2228224;</summary>
        public static int GLY_RBWX_OFFSET = 2228224;
        /// <summary>GLY_RCWX_OFFSET = 2293760;</summary>
        public static int GLY_RCWX_OFFSET = 2293760;
        /// <summary>GLY_USF_OFFSET = 2359296;</summary>
        public static int GLY_USF_OFFSET = 2359296;
        /// <summary>GLY_JAVA_IF_OFFSET = 2424832;</summary>
        public static int GLY_JAVA_IF_OFFSET = 2424832;
        /// <summary>GLY_SOAP_SERVICE_OFFSET = 2490368;</summary>
        public static int GLY_SOAP_SERVICE_OFFSET = 2490368;
        /// <summary>GLY_SDRB100_OFFSET = 2555904;</summary>
        public static int GLY_SDRB100_OFFSET = 2555904;
        /// <summary>GLY_SDRC100_OFFSET = 2621440;</summary>
        public static int GLY_SDRC100_OFFSET = 2621440;
        /// <summary>GLY_DSC_OFFSET = 2752512;</summary>
        public static int GLY_DSC_OFFSET = 2752512;
        /// <summary>GLY_DSB_OFFSET = 2760704;</summary>
        public static int GLY_DSB_OFFSET = 2760704;
        /// <summary>GLY_PBNX_OFFSET = 2818048;</summary>
        public static int GLY_PBNX_OFFSET = 2818048;
        /// <summary>GLY_HVE200_OFFSET = 3211264;</summary>
        public static int GLY_HVE200_OFFSET = 3211264;
        /// <summary>GLY_RBW150_OFFSET = 3276800;</summary>
        public static int GLY_RBW150_OFFSET = 3276800;
        /// <summary>GLY_SCWX_OFFSET = 3342336;</summary>
        public static int GLY_SCWX_OFFSET = 3342336;
        /// <summary>GLY_SUCCESS = 0;</summary>
        public static int GLY_SUCCESS = 0;
        /// <summary>GLY_COMPLETE_MESSAGE = 1;</summary>
        public const int GLY_COMPLETE_MESSAGE = 1;
        /// <summary>GLY_CANCELED = 2;</summary>
        public static int GLY_CANCELED = 2;
        /// <summary>GLY_DEVICE_SPECIFIC_EVENT = 3;</summary>
        public static int GLY_DEVICE_SPECIFIC_EVENT = 3;
        /// <summary>GLY_DEVICE_SPECIFIC_EVENT_BY_STRING = 4;</summary>
        public static int GLY_DEVICE_SPECIFIC_EVENT_BY_STRING = 4;
        /// <summary>GLY_HANDLE_CLOSED = 5;</summary>
        public static int GLY_HANDLE_CLOSED = 5;
        /// <summary>GLY_DEVICE_SPECIFIC_EVENT_EX = 6;</summary>
        public static int GLY_DEVICE_SPECIFIC_EVENT_EX = 6;
        /// <summary>GLY_STATUS_CHANGE = 2001;</summary>
        public static int GLY_STATUS_CHANGE = 2001;
        /// <summary>GLY_EMPTY = 2002;</summary>
        public const int GLY_EMPTY = 2002;
        /// <summary>GLY_EXIST = 2003;</summary>
        public static int GLY_EXIST = 2003;
        /// <summary>GLY_FULL = 2004;</summary>
        public static int GLY_FULL = 2004;
        /// <summary>GLY_ERROR = 2005;</summary>
        public static int GLY_ERROR = 2005;
        /// <summary>GLY_STATUS_IDLE = 2006;</summary>
        public static int GLY_STATUS_IDLE = 2006;
        /// <summary>GLY_STATUS_USING_OWN = 2007;</summary>
        public static int GLY_STATUS_USING_OWN = 2007;
        /// <summary>GLY_STATUS_BUSY = 2008;</summary>
        public static int GLY_STATUS_BUSY = 2008;
        /// <summary>GLY_STATUS_ERROR = 2009;</summary>
        public static int GLY_STATUS_ERROR = 2009;
        /// <summary>GLY_STATUS_ERROR_COMMUNICATION = 2010;</summary>
        public static int GLY_STATUS_ERROR_COMMUNICATION = 2010;
        /// <summary>GLY_DEPOSIT_COUNT_CHANGE = 2011;</summary>
        public const int GLY_DEPOSIT_COUNT_CHANGE = 2011;
        /// <summary>GLY_REPLENISH_COUNT_CHANGE = 2012;</summary>
        public static int GLY_REPLENISH_COUNT_CHANGE = 2012;
        /// <summary>GLY_NUMBER_COUNTING_COUNT_CHANGE = 2013;</summary>
        public static int GLY_NUMBER_COUNTING_COUNT_CHANGE = 2013;
        /// <summary>GLY_STATUS_DLL_INITIALIZE_BUSY = 2015;</summary>
        public static int GLY_STATUS_DLL_INITIALIZE_BUSY = 2015;
        /// <summary>GLY_HIGH = 2017;</summary>
        public static int GLY_HIGH = 2017;
        /// <summary>GLY_STOP = 2018;</summary>
        public static int GLY_STOP = 2018;
        /// <summary>GLY_MISSING = 2020;</summary>
        public static int GLY_MISSING = 2020;
        /// <summary>GLY_SHORTAGE = 2021;</summary>
        public static int GLY_SHORTAGE = 2021;
        /// <summary>GLY_INTERMEDIATE_COLLECT_COUNT_CHANGE = 2025;</summary>
        public static int GLY_INTERMEDIATE_COLLECT_COUNT_CHANGE = 2025;
        /// <summary>GLY_CLOSED = 2030;</summary>
        public static int GLY_CLOSED = 2030;
        /// <summary>GLY_OPENED = 2031;</summary>
        public static int GLY_OPENED = 2031;
        /// <summary>GLY_VERIFY_COUNT_CHANGE = 2016;</summary>
        public static int GLY_VERIFY_COUNT_CHANGE = 2016;
        /// <summary>GLY_REFILL_COUNT_CHANGE = 2024;</summary>
        public static int GLY_REFILL_COUNT_CHANGE = 2024;
        /// <summary>GLY_COLLECT_COUNT_CHANGE = 2032;</summary>
        public static int GLY_COLLECT_COUNT_CHANGE = 2032;
        /// <summary>GLY_LOW = 2014;</summary>
        public static int GLY_LOW = 2014;
        /// <summary>GLY_NA = 2023;</summary>
        public static int GLY_NA = 2023;
        /// <summary>GLY_WAIT_FOR_REMOVING = 3001;</summary>
        public static int GLY_WAIT_FOR_REMOVING = 3001;
        /// <summary>GLY_REMOVED = 3002;</summary>
        public static int GLY_REMOVED = 3002;
        /// <summary>GLY_POWER_OFF_ON_REQUEST = 3003;</summary>
        public static int GLY_POWER_OFF_ON_REQUEST = 3003;
        /// <summary>GLY_PUSH_INUSE_KEY = 3004;</summary>
        public static int GLY_PUSH_INUSE_KEY = 3004;
        /// <summary>GLY_ISSUE_RESET_COMMAND = 3006;</summary>
        public static int GLY_ISSUE_RESET_COMMAND = 3006;
        /// <summary>GLY_ENTRANCE = 4001;</summary>
        public static int GLY_ENTRANCE = 4001;
        /// <summary>GLY_EXIT = 4002;</summary>
        public static int GLY_EXIT = 4002;
        /// <summary>GLY_ESCROW = 4003;</summary>
        public static int GLY_ESCROW = 4003;
        /// <summary>GLY_STACKER1 = 4004;</summary>
        public static int GLY_STACKER1 = 4004;
        /// <summary>GLY_STACKER2 = 4005;</summary>
        public static int GLY_STACKER2 = 4005;
        /// <summary>GLY_STACKER3 = 4006;</summary>
        public static int GLY_STACKER3 = 4006;
        /// <summary>GLY_STACKER4 = 4007;</summary>
        public static int GLY_STACKER4 = 4007;
        /// <summary>GLY_STACKER5 = 4008;</summary>
        public static int GLY_STACKER5 = 4008;
        /// <summary>GLY_STACKER6 = 4009;</summary>
        public static int GLY_STACKER6 = 4009;
        /// <summary>GLY_RJB = 4010;</summary>
        public static int GLY_RJB = 4010;
        /// <summary>GLY_CASSETTE1 = 4025;</summary>
        public static int GLY_CASSETTE1 = 4025;
        /// <summary>GLY_CASSETTE2 = 4026;</summary>
        public static int GLY_CASSETTE2 = 4026;
        /// <summary>GLY_CASSETTE3 = 4027;</summary>
        public static int GLY_CASSETTE3 = 4027;
        /// <summary>GLY_CASSETTE4 = 4028;</summary>
        public static int GLY_CASSETTE4 = 4028;
        /// <summary>GLY_CASSETTE5 = 4029;</summary>
        public static int GLY_CASSETTE5 = 4029;
        /// <summary>GLY_CASSETTE6 = 4030;</summary>
        public static int GLY_CASSETTE6 = 4030;
        /// <summary>GLY_DOWNLOAD_PROGRESS = 4141;</summary>
        public static int GLY_DOWNLOAD_PROGRESS = 4141;
        /// <summary>GLY_LOGREAD_PROGRESS = 4142;</summary>
        public static int GLY_LOGREAD_PROGRESS = 4142;
        /// <summary>GLY_SAFE_DOOR = 4155;</summary>
        public static int GLY_SAFE_DOOR = 4155;
        /// <summary>GLY_CASSETTE = 4166;</summary>
        public static int GLY_CASSETTE = 4166;
        /// <summary>GLY_STACKER7 = 4177;</summary>
        public static int GLY_STACKER7 = 4177;
        /// <summary>GLY_STACKER8 = 4178;</summary>
        public static int GLY_STACKER8 = 4178;
        /// <summary>GLY_EXIT_RJ = 4201;</summary>
        public static int GLY_EXIT_RJ = 4201;
        /// <summary>GLY_CASSETTE7 = 4202;</summary>
        public static int GLY_CASSETTE7 = 4202;
        /// <summary>GLY_CASSETTE8 = 4203;</summary>
        public static int GLY_CASSETTE8 = 4203;
        /// <summary>GLY_CATEGORY_1 = 4211;</summary>
        public static int GLY_CATEGORY_1 = 4211;
        /// <summary>GLY_CATEGORY_2 = 4212;</summary>
        public static int GLY_CATEGORY_2 = 4212;
        /// <summary>GLY_CATEGORY_3 = 4213;</summary>
        public static int GLY_CATEGORY_3 = 4213;
        /// <summary>GLY_CATEGORY_4A = 4214;</summary>
        public static int GLY_CATEGORY_4A = 4214;
        /// <summary>GLY_CATEGORY_4B = 4215;</summary>
        public static int GLY_CATEGORY_4B = 4215;
        /// <summary>GET_IGNORE_STATE_FOR_OCX = 5010;</summary>
        public static int GET_IGNORE_STATE_FOR_OCX = 5010;
        /// <summary>GLY_CPS1_DISPENSE1 = 7001;</summary>
        public static int GLY_CPS1_DISPENSE1 = 7001;
        /// <summary>GLY_CPS2_DISPENSE1 = 7002;</summary>
        public static int GLY_CPS2_DISPENSE1 = 7002;
        /// <summary>GLY_CPS3_DISPENSE1 = 7003;</summary>
        public static int GLY_CPS3_DISPENSE1 = 7003;
        /// <summary>GLY_CPS4_DISPENSE1 = 7004;</summary>
        public static int GLY_CPS4_DISPENSE1 = 7004;
        /// <summary>GLY_CPS5_DISPENSE1 = 7005;</summary>
        public static int GLY_CPS5_DISPENSE1 = 7005;
        /// <summary>GLY_CPS6_DISPENSE1 = 7006;</summary>
        public static int GLY_CPS6_DISPENSE1 = 7006;
        /// <summary>GLY_CPS1_DISPENSE2 = 7007;</summary>
        public static int GLY_CPS1_DISPENSE2 = 7007;
        /// <summary>GLY_CPS2_DISPENSE2 = 7008;</summary>
        public static int GLY_CPS2_DISPENSE2 = 7008;
        /// <summary>GLY_CPS3_DISPENSE2 = 7009;</summary>
        public static int GLY_CPS3_DISPENSE2 = 7009;
        /// <summary>GLY_CPS4_DISPENSE2 = 7010;</summary>
        public static int GLY_CPS4_DISPENSE2 = 7010;
        /// <summary>GLY_CPS5_DISPENSE2 = 7011;</summary>
        public static int GLY_CPS5_DISPENSE2 = 7011;
        /// <summary>GLY_CPS6_DISPENSE2 = 7012;</summary>
        public static int GLY_CPS6_DISPENSE2 = 7012;
        /// <summary>GLY_CPS1_COUNTER = 7013;</summary>
        public static int GLY_CPS1_COUNTER = 7013;
        /// <summary>GLY_CPS2_COUNTER = 7014;</summary>
        public static int GLY_CPS2_COUNTER = 7014;
        /// <summary>GLY_CPS3_COUNTER = 7015;</summary>
        public static int GLY_CPS3_COUNTER = 7015;
        /// <summary>GLY_CPS4_COUNTER = 7016;</summary>
        public static int GLY_CPS4_COUNTER = 7016;
        /// <summary>GLY_CPS5_COUNTER = 7017;</summary>
        public static int GLY_CPS5_COUNTER = 7017;
        /// <summary>GLY_CPS6_COUNTER = 7018;</summary>
        public static int GLY_CPS6_COUNTER = 7018;
        /// <summary>GLY_CPS7_COUNTER = 7019;</summary>
        public static int GLY_CPS7_COUNTER = 7019;
        /// <summary>GLY_CPS8_COUNTER = 7020;</summary>
        public static int GLY_CPS8_COUNTER = 7020;
        /// <summary>GLY_CONTAINER_C4A_COUNTER = 7021;</summary>
        public static int GLY_CONTAINER_C4A_COUNTER = 7021;
        /// <summary>GLY_DEPOSIT_COUNT_MONITOR = 7022;</summary>
        public static int GLY_DEPOSIT_COUNT_MONITOR = 7022;
        /// <summary>GLY_CONTAINER_C1_COUNTER = 7023;</summary>
        public static int GLY_CONTAINER_C1_COUNTER = 7023;
        /// <summary>GLY_CONTAINER_C2_COUNTER = 7024;</summary>
        public static int GLY_CONTAINER_C2_COUNTER = 7024;
        /// <summary>GLY_CONTAINER_C3_COUNTER = 7025;</summary>
        public static int GLY_CONTAINER_C3_COUNTER = 7025;
        /// <summary>GLY_CPS1_C3_COUNTER = 7026;</summary>
        public static int GLY_CPS1_C3_COUNTER = 7026;
        /// <summary>GLY_CPS2_C3_COUNTER = 7027;</summary>
        public static int GLY_CPS2_C3_COUNTER = 7027;
        /// <summary>GLY_CPS3_C3_COUNTER = 7028;</summary>
        public static int GLY_CPS3_C3_COUNTER = 7028;
        /// <summary>GLY_CPS4_C3_COUNTER = 7029;</summary>
        public static int GLY_CPS4_C3_COUNTER = 7029;
        /// <summary>GLY_CPS6_C3_COUNTER = 7030;</summary>
        public static int GLY_CPS6_C3_COUNTER = 7030;
        /// <summary>GLY_CPS8_C3_COUNTER = 7031;</summary>
        public static int GLY_CPS8_C3_COUNTER = 7031;
        /// <summary>GLY_CAPTUREBIN_COUNTER = 7032;</summary>
        public static int GLY_CAPTUREBIN_COUNTER = 7032;
        /// <summary>GLY_CURRENCY_DEVICE = 7033;</summary>
        public static int GLY_CURRENCY_DEVICE = 7033;
        /// <summary>GLY_NUM_DEVICE_EXIT = 7034;</summary>
        public static int GLY_NUM_DEVICE_EXIT = 7034;
        /// <summary>GLY_MAX_DISPENSE_CNT = 7035;</summary>
        public static int GLY_MAX_DISPENSE_CNT = 7035;
        /// <summary>GLY_CONTAINER_C4B_COUNTER = 7036;</summary>
        public static int GLY_CONTAINER_C4B_COUNTER = 7036;
        /// <summary>GLY_STATUS_COUNTING = 7037;</summary>
        public static int GLY_STATUS_COUNTING = 7037;
        /// <summary>GLY_COLLECTION_BOX = 7038;</summary>
        public static int GLY_COLLECTION_BOX = 7038;
        /// <summary>GLY_COLLECT_BOX_INSERTED = 7039;</summary>
        public static int GLY_COLLECT_BOX_INSERTED = 7039;
        /// <summary>GLY_REJECT_BOX = 7040;</summary>
        public static int GLY_REJECT_BOX = 7040;
        /// <summary>GLY_CAP_CANCEL_DEPOSIT = 7053;</summary>
        public static int GLY_CAP_CANCEL_DEPOSIT = 7053;
        /// <summary>GLY_CAP_PAUSE_DEPOSIT = 7054;</summary>
        public static int GLY_CAP_PAUSE_DEPOSIT = 7054;
        /// <summary>GLY_CAP_COMPARE_FIRM = 7055;</summary>
        public static int GLY_CAP_COMPARE_FIRM = 7055;
        /// <summary>GLY_CAP_UPDATE_FIRM = 7056;</summary>
        public static int GLY_CAP_UPDATE_FIRM = 7056;
        /// <summary>GLY_CAP_LOG_READ = 7057;</summary>
        public static int GLY_CAP_LOG_READ = 7057;
        /// <summary>GLY_CAP_LOG_CLEAR = 7058;</summary>
        public static int GLY_CAP_LOG_CLEAR = 7058;
        /// <summary>GLY_CAP_DEPOSIT = 7059;</summary>
        public static int GLY_CAP_DEPOSIT = 7059;
        /// <summary>GLY_CAP_DESCREPANCY = 7060;</summary>
        public static int GLY_CAP_DESCREPANCY = 7060;
        /// <summary>GLY_P_FIRMWARE_UNKNOWN = 7061;</summary>
        public static int GLY_P_FIRMWARE_UNKNOWN = 7061;
        /// <summary>GLY_P_FIRMWARE_SAME = 7062;</summary>
        public static int GLY_P_FIRMWARE_SAME = 7062;
        /// <summary>GLY_P_FIRMWARE_NEWER = 7063;</summary>
        public static int GLY_P_FIRMWARE_NEWER = 7063;
        /// <summary>GLY_P_FIRMWARE_OLDER = 7064;</summary>
        public static int GLY_P_FIRMWARE_OLDER = 7064;
        /// <summary>GLY_P_FIRMWARE_DIFFERENT = 7065;</summary>
        public static int GLY_P_FIRMWARE_DIFFERENT = 7065;
        /// <summary>GLY_CHECK_COUNTER = 7066;</summary>
        public static int GLY_CHECK_COUNTER = 7066;
        /// <summary>DepositToCollection = 7067;</summary>
        public static int DepositToCollection = 7067;
        /// <summary>GLY_RBG_NOTE_DATA_INFO = 7068;</summary>
        public static int GLY_RBG_NOTE_DATA_INFO = 7068;
        /// <summary>GLY_EXPOSED_FUND_COUNTER = 7069;</summary>
        public static int GLY_EXPOSED_FUND_COUNTER = 7069;
        /// <summary>GLY_INDEFINITE_WAIT = -1;</summary>
        public static int GLY_INDEFINITE_WAIT = -1;
        /// <summary>GLY_ERROR_SYSTEM_ERROR = -1;</summary>
        public static int GLY_ERROR_SYSTEM_ERROR = -1;
        /// <summary>GLY_ERROR_PORT_OPEN_FAIL = -2;</summary>
        public static int GLY_ERROR_PORT_OPEN_FAIL = -2;
        /// <summary>GLY_ERROR_PORT_CONFIG_ERROR = -3;</summary>
        public static int GLY_ERROR_PORT_CONFIG_ERROR = -3;
        /// <summary>GLY_ERROR_DEVICE_NOT_FOUND = -4;</summary>
        public static int GLY_ERROR_DEVICE_NOT_FOUND = -4;
        /// <summary>GLY_ERROR_PORT_NOT_FOUND = -5;</summary>
        public static int GLY_ERROR_PORT_NOT_FOUND = -5;
        /// <summary>GLY_ERROR_TCP_HOST_NOT_FOUND = -6;</summary>
        public static int GLY_ERROR_TCP_HOST_NOT_FOUND = -6;
        /// <summary>GLY_ERROR_INVALID_HANDLE = -7;</summary>
        public static int GLY_ERROR_INVALID_HANDLE = -7;
        /// <summary>GLY_ERROR_INVALID_MESSAGE_CODE = -8;</summary>
        public static int GLY_ERROR_INVALID_MESSAGE_CODE = -8;
        /// <summary>GLY_ERROR_USER_WINDOW_MESSAGE_NOT_SET = -9;</summary>
        public static int GLY_ERROR_USER_WINDOW_MESSAGE_NOT_SET = -9;
        /// <summary>GLY_ERROR_NOT_SUPPORTED_FUNCTION = -10;</summary>
        public static int GLY_ERROR_NOT_SUPPORTED_FUNCTION = -10;
        /// <summary>GLY_ERROR_INVALID_ID = -11;</summary>
        public static int GLY_ERROR_INVALID_ID = -11;
        /// <summary>GLY_ERROR_NOT_SUPPORTED_CURRENCYID = -12;</summary>
        public static int GLY_ERROR_NOT_SUPPORTED_CURRENCYID = -12;
        /// <summary>GLY_ERROR_UDP_HOST_NOT_FOUND = -13;</summary>
        public static int GLY_ERROR_UDP_HOST_NOT_FOUND = -13;
        /// <summary>GLY_ERROR_ID_NOT_FOUND = -14;</summary>
        public static int GLY_ERROR_ID_NOT_FOUND = -14;
        /// <summary>GLY_ERROR_NOT_OPEN = -15;</summary>
        public static int GLY_ERROR_NOT_OPEN = -15;
        /// <summary>GLY_ERROR_ALREADY_OPEN = -16;</summary>
        public static int GLY_ERROR_ALREADY_OPEN = -16;
        /// <summary>GLY_ERROR_DLL_NOT_FOUND = -18;</summary>
        public static int GLY_ERROR_DLL_NOT_FOUND = -18;
        /// <summary>GLY_ERROR_DEVICE_CONFIG_ERROR = -19;</summary>
        public static int GLY_ERROR_DEVICE_CONFIG_ERROR = -19;
        /// <summary>GLY_ERROR_FILE_NOT_FOUND = -20;</summary>
        public static int GLY_ERROR_FILE_NOT_FOUND = -20;
        /// <summary>GLY_ERROR_ELEMENT_NOT_FOUND = -21;</summary>
        public static int GLY_ERROR_ELEMENT_NOT_FOUND = -21;
        /// <summary>GLY_ERROR_FILE_FORMAT = -22;</summary>
        public static int GLY_ERROR_FILE_FORMAT = -22;
        /// <summary>GLY_ERROR_DL_FILE_SUM_CHECK_ERROR = -23;</summary>
        public static int GLY_ERROR_DL_FILE_SUM_CHECK_ERROR = -23;
        /// <summary>GLY_ERROR_DL_NEED_AGAIN = -24;</summary>
        public static int GLY_ERROR_DL_NEED_AGAIN = -24;
        /// <summary>GLY_ERROR_LOGICAL_NAME_NOT_DEFINED = -25;</summary>
        public static int GLY_ERROR_LOGICAL_NAME_NOT_DEFINED = -25;
        /// <summary>GLY_ERROR_CONFIG_FILE_NOT_FOUND = -26;</summary>
        public static int GLY_ERROR_CONFIG_FILE_NOT_FOUND = -26;
        /// <summary>GLY_ERROR_INVALID_DLL = -27;</summary>
        public static int GLY_ERROR_INVALID_DLL = -27;
        /// <summary>GLY_ERROR_SEQUENCE = -101;</summary>
        public static int GLY_ERROR_SEQUENCE = -101;
        /// <summary>GLY_ERROR_COMMUNICATION = -102;</summary>
        public static int GLY_ERROR_COMMUNICATION = -102;
        /// <summary>GLY_ERROR_BUSY = -103;</summary>
        public static int GLY_ERROR_BUSY = -103;
        /// <summary>GLY_ERROR_MECHA = -104;</summary>
        public static int GLY_ERROR_MECHA = -104;
        /// <summary>GLY_ERROR_OPERATION = -105;</summary>
        public static int GLY_ERROR_OPERATION = -105;
        /// <summary>GLY_ERROR_PARAMETER = -106;</summary>
        public static int GLY_ERROR_PARAMETER = -106;
        /// <summary>GLY_ERROR_INVALID_PARAMETER = -107;</summary>
        public static int GLY_ERROR_INVALID_PARAMETER = -107;
        /// <summary>GLY_ERROR_MONEY_NOT_MATCHED = -108;</summary>
        public static int GLY_ERROR_MONEY_NOT_MATCHED = -108;
        /// <summary>GLY_ERROR_NOT_LOCKED = -109;</summary>
        public static int GLY_ERROR_NOT_LOCKED = -109;
        /// <summary>GLY_ERROR_TIMEOUT = -110;</summary>
        public static int GLY_ERROR_TIMEOUT = -110;
        /// <summary>GLY_ERROR_COM_PORT_IS_OCCUPIED = -111;</summary>
        public static int GLY_ERROR_COM_PORT_IS_OCCUPIED = -111;
        /// <summary>GLY_ERROR_INCOMPLETE = -112;</summary>
        public static int GLY_ERROR_INCOMPLETE = -112;
        /// <summary>GLY_ERROR_SHORTAGE = -113;</summary>
        public static int GLY_ERROR_SHORTAGE = -113;
        /// <summary>GLY_ERROR_SYSTEM_LOCKED = -114;</summary>
        public static int GLY_ERROR_SYSTEM_LOCKED = -114;
        /// <summary>GLY_ERROR_NOT_EJECT_BAIT = -115;</summary>
        public static int GLY_ERROR_NOT_EJECT_BAIT = -115;
        /// <summary>GLY_ERROR_REMOVE_END_STACKER = -116;</summary>
        public static int GLY_ERROR_REMOVE_END_STACKER = -116;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_401 = -1001;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_401 = -1001;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_402 = -1002;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_402 = -1002;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_403 = -1003;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_403 = -1003;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_404 = -1004;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_404 = -1004;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_405 = -1005;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_405 = -1005;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_406 = -1006;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_406 = -1006;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_407 = -1007;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_407 = -1007;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_408 = -1008;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_408 = -1008;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_409 = -1009;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_409 = -1009;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_410 = -1010;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_410 = -1010;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_411 = -1011;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_411 = -1011;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_412 = -1012;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_412 = -1012;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_413 = -1013;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_413 = -1013;
        /// <summary>GLY_ERROR_COMMUNICATION_TYPED_414 = -1014;</summary>
        public static int GLY_ERROR_COMMUNICATION_TYPED_414 = -1014;
        /// <summary>GLY_ERROR_BILL_COUNTS = 1048598;</summary>
        public static int GLY_ERROR_BILL_COUNTS = 1048598;
        /// <summary>BillsToCaptureBin = 7041;</summary>
        public static int BillsToCaptureBin = 7041;
        /// <summary>RZ_GetSerialNumber = 7042;</summary>
        public static int RZ_GetSerialNumber = 7042;
        /// <summary>RZ_GetCstSerialNumber = 7043;</summary>
        public static int RZ_GetCstSerialNumber = 7043;
        /// <summary>RZ_SetSerialNumber = 7044;</summary>
        public static int RZ_SetSerialNumber = 7044;
        /// <summary>RZ_SetCstSerialNumber = 7045;</summary>
        public static int RZ_SetCstSerialNumber = 7045;
        /// <summary>DirectDispense = 7046;</summary>
        public static int DirectDispense = 7046;
        /// <summary>RejectDataRead = 7047;</summary>
        public static int RejectDataRead = 7047;
        /// <summary>NoteDataRead = 7048;</summary>
        public static int NoteDataRead = 7048;
        /// <summary>ModifyMessage = 7049;</summary>
        public static int ModifyMessage = 7049;
        /// <summary>WriteInformation = 7050;</summary>
        public static int WriteInformation = 7050;
        /// <summary>ReadInformation = 7051;</summary>
        public static int ReadInformation = 7051;
        /// <summary>GetCountNumber = 7052;</summary>
        public static int GetCountNumber = 7052;
        /// <summary>RZ_Collect34B = 1245197;</summary>
        public static int RZ_Collect34B = 1245197;
        /// <summary>RemainsCollect = 1245190;</summary>
        public static int RemainsCollect = 1245190;
        /// <summary>GLY_BALANCE_COUNTER = 1507345;</summary>
        public static int GLY_BALANCE_COUNTER = 1507345;
        /// <summary>GLY_COUNT_START = 1507329;</summary>
        public static int GLY_COUNT_START = 1507329;
        /// <summary>GLY_COUNTER_CORRECTION_COUNT_CHANGE = 1048597;</summary>
        public static int GLY_COUNTER_CORRECTION_COUNT_CHANGE = 1048597;
        #endregion GLYCODE INT
    }
}
