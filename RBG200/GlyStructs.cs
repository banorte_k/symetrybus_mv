﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RBG200
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYPARAM
    {
        public IntPtr handle;
        public uint errorCode;
        public uint messageId;
        public int requestId;
        public int size;
        public IntPtr buffer;
        public IntPtr paramName;
        public int apiId;
        public IntPtr glyParam;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYDENOMINATION
    {
        public uint arraySize;
        public IntPtr currencies;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYCURRENCY
    {
        public uint value;
        public uint counts;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public char[] currencyID;
        public int rev;
        public GLYVALUEEXP valueExp;
        public int category;
        public IntPtr signature;
        public IntPtr misc;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYVALUEEXP
    {
        public int factor;
        public int exp;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYCOUNTRESULT
    {
        public GLYDENOMINATION C4;
        public GLYDENOMINATION C3;
        public GLYDENOMINATION C2;
        public GLYDENOMINATION C1;
        public GLYDENOMINATION C4B;
        public GLYDENOMINATION C4A;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYCOMMONCOUNTERS
    {
        public IntPtr lpDepositCounter;
        public IntPtr lpDispenseCounter;
        public IntPtr lpBalanceCounter;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYCOMMONCOUNTER
    {
        public uint arraySize;
        public IntPtr commonCurrency;

    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYCOMMONCURRENCY
    {
        public uint dwStatus;
        public GLYCURRENCY Currency;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct GLYRBG200UNITSTATUS
    {
        public int RequestOfAutoVerification;
        public int ModuleStatus;
        public int Restrictions;
        public int SettingSwitch;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
        public char[] SensorDust;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public char[] Dummy1;
        public int PowerSavingMode;
        public int CassetteIdCheck;
        public int SystemBlock;
        public int NonusableModule;
        public int ErrorCode;
        public int StepCodeAtError;
    }
}
