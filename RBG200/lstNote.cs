﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RBG200
{
    public class lstNote
    {
        string note;
        string count;

        public lstNote(string note, string count)
        {
            this.note = note;
            this.count = count;
        }
        public string Note
        {
            get
            {
                return note;
            }

            set
            {
                note = value;
            }
        }
        public string Count
        {
            get
            {
                return count;
            }

            set
            {
                count = value;
            }
        }
    }
}
