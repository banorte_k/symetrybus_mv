﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RBG200
{
    public class GlyDllApi
    {
        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_Open([MarshalAs(UnmanagedType.LPStr)] string lpString, ref IntPtr glyHandle);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_SetCallBack(IntPtr glyHandle, Delegate glyNotification);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_Close(IntPtr glyHandle);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncDirectDeposit(IntPtr glyHandle, Delegate glyNotification, ref IntPtr lpRequestID);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncStore(IntPtr glyHandle, Delegate glyNotification, ref IntPtr lpRequestID);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncAutoRepetitiveDeposit(IntPtr glyHandle, Delegate glyNotification, ref IntPtr lpRequestID);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncCancelInterruptDeposit(IntPtr glyHandle, Delegate glyNotification, ref IntPtr lpRequestID);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_WriteTrace(string str);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_GetStatus(IntPtr glyHandle, ref IntPtr lpdwStatus);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncDispense(IntPtr glyHandle, Delegate glyNotification, ref IntPtr lpRequestID, IntPtr lpDenomination);


        [DllImport("libglyentrycollection.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_RBG200_GetUnitStatus(IntPtr glyHandle, ref GLYRBG200UNITSTATUS lpUnitStatus);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncCollect(IntPtr glyHandl, Delegate glyNotification, ref IntPtr lpRequestID);

        [DllImport("libglorycolx2010.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GLY_AsyncPresent(IntPtr glyHandle, Delegate glyNotification, ref IntPtr lpRequestID);
    }
}
